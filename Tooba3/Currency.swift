//
//  Currency.swift
//  Tooba
//
//  Created by Admin on 26/07/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import Foundation

enum Currency {
    case none
    case dollar
    case euro
    case ruble
    
    func toString(_ value: Double) -> String {
        let format = (value - round(value) < 0.01 ? "%.0f" : "%.2f") as NSString
        switch self {
        case .euro:
            return "€\(NSString(format: format, value) as String)"
        case .dollar:
            return "$\(NSString(format: format, value) as String)"
        case .ruble:
            return "\(NSString(format: format, value) as String) ₽"
        default:
            return "\(NSString(format: format, value) as String)"
        }
    }
    
    static func fromString(s : String) -> Currency {
        var result = Currency.none
        switch s.lowercased() {
        case "dollar":
            result = .dollar
        case "euro":
            result = .euro
        case "ruble":
            result = .ruble
        default:
            result = .none
        }
        return result
    }

    func getServerCode() -> Int {
        switch self {
        case .dollar:
            return 1
        case .euro:
            return 0
        case .ruble:
            return 2
        default:
            return -1
        }
    }

    static func fromServerCode(code : Int) -> Currency {
        var result = Currency.none
        switch code {
        case 1:
            result = .dollar
        case 0:
            result = .euro
        case 2:
            result = .ruble
        default:
            result = .none
        }
        return result
    }
}
