//
//  BankCardCell.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 04/10/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class BankCardCell : UITableViewCell {
    public var onDeleteTouched: (() -> ())? = nil
    
    @IBAction func btnDeleteTouched(_ sender: Any) {
        self.onDeleteTouched?()
    }
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var label: UILabel!
    
    
}
