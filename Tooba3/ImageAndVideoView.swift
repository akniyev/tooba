//
//  VideoPlayerView.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 23/02/2019.
//  Copyright © 2019 greenworld. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

enum ImageVideoTypes {
    case Image
    case Video
}

class ImageAndVideoView : UIView, WKYTPlayerViewDelegate  {
    @IBOutlet var view: UIView!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var video: WKYTPlayerView!
    
    var mediaType : ImageVideoTypes = .Image
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func playVideo() {
        if self.mediaType != .Video { return }
        self.video.playVideo()
    }
    
    func stopVideo() {
        if self.mediaType != .Video { return }
        self.video.stopVideo()
    }
    
    func setup() {
        view = loadViewFromNib()
        view.frame = bounds
        addSubview(view)
        self.video.delegate = self
        self.video.isHidden = true
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    func setImage(url: String) {
        self.video.isHidden = true
        self.previewImage.kf.setImage(with: URL(string: url))
        self.activityIndicator.stopAnimating()
        self.mediaType = .Image
    }
    
    func setVideo(code: String) {
        self.video.isHidden = true
        let url = "https://img.youtube.com/vi/\(code)/0.jpg"
        self.previewImage.kf.setImage(with: URL(string: url))
        self.activityIndicator.startAnimating()
        self.mediaType = .Video
        
        self.video.load(withVideoId: code, playerVars: [
            "playsinline" : 1,
            "controls" : 1,
            "modestbranding" : 1,
            "fs" : 1,
            "showinfo" : 1,
            ])
    }
    
    // Player delegate
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        self.video.isHidden = false
        self.video.playVideo()
        self.video.stopVideo()
    }
    
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        if state == .ended {
            print("Ended")
        }
    }
}
