//
//  BankCardsViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 28/10/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import UIKit

class BankCardsViewController : UITableViewController {
    static func storyboardInstance() -> BankCardsViewController? {
        let storyboard = UIStoryboard(name: "Payment", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "BankCardsViewController") as? BankCardsViewController
    }
    
    var bankCards: [BankCardItem] = []

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bankCards.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BankCardCell") ?? UITableViewCell()
        
        cell.textLabel?.text = bankCards[indexPath.row].cardMask
        
        
        return cell
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Alerts.ShowAlert(sender: self, title: "ATTENTION".localized, message: "DO_U_WANT_TO_DELETE_THIS_CARD".localized, preferredStyle: .alert, actions: [
                UIAlertAction(title: "YES".localized, style: .default) { [unowned self] action in
                    self.view.showReloadView()
                    Server.YandexKassaDeleteLinkedBankCard(cardMask: self.bankCards[indexPath.row].cardMask, SUCCESS: {
                        self.view.hideReloadView()
                        self.reloadDataFromServer()
                    }, ERROR: {
                        self.view.hideReloadView()
                        Alerts.showErrorAlertWithOK(sender: self, title: "ERROR".localized, message: "CANT_DELETE_BANK_CARD".localized, completion: nil)
                    })
                },
                UIAlertAction(title: "NO".localized, style: .default) { [unowned self] action in
                    tableView.setEditing(false, animated: true)
                }
            ], completion: nil)
        }
    }

    func reloadDataFromServer() {
        self.view.ShowWhiteReloadView()
        Server.yandexKassaLinkedBankCards(successCompletion: { [unowned self] bankCards in
            self.bankCards = bankCards
            self.tableView.reloadData()
            self.view.hideReloadView()
        }, onErrorCompletion: {
            self.view.hideReloadView()
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadDataFromServer()
    }
}
