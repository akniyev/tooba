//
//  DatePicker.swift
//  Tooba3
//
//  Created by Admin on 21/09/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class DatePicker: UIView {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBAction func pickerValueChanged(_ sender: AnyObject) {
        date = picker.date
        updateDate()
    }
    
    @IBAction func btnConfirmTouched(_ sender: AnyObject) {
        if let e = confirmPressed {
            e(date)
        }
    }
    
    var date : Date? = nil
    
    var confirmPressed : ((_ x : Date?) -> ())?
    
    @IBAction func btnClearTouched(_ sender: AnyObject) {
        date = nil
        updateDate()
    }
    
    func setDate(nd : Date?) {
        date = nd
        if let d = nd {
            picker.date = d
        }
        updateDate()
    }

    func updateDate() {
        if let d = date {
            lblDate.text = d.description
        } else {
            lblDate.text = "No date"
        }
    }
    
    func myInit() {
        if let d = date {
            picker.date = d
        } else {
            picker.date = Date()
        }
        
        picker.maximumDate = Date()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
