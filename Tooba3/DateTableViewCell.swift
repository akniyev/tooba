//
//  DateTableViewCell.swift
//  Tooba3
//
//  Created by Admin on 21/09/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class DateTableViewCell: UITableViewCell, CellInfoReturner {
    @IBOutlet weak var lblLabel: UILabel!
    @IBOutlet weak var btnSelectDate: UIButton!
    
    func getCellData() -> CellInfo {
        return CellInfo(type: .DateTime, label: lblLabel.text!, date: date)
    }
    
    var date : Date? = nil
    
    func setDate(nd : Date?) {
        date = nd
        if let d = date {
            btnSelectDate.setTitle(d.description, for: .normal)
        } else {
            btnSelectDate.setTitle("No date", for: .normal)
        }
    }
    
    
    
    var bv : UIView? = nil
    var v : DatePicker? = nil
    
    @IBAction func btnSelectDateTouched(_ sender: AnyObject) {
        let vs = Bundle.main.loadNibNamed("DatePicker", owner: self, options: nil) as! [DatePicker]
        if vs.count > 0 {
            bv = UIView()
            bv?.backgroundColor = UIColor.white
            v = vs[0]
            v!.setDate(nd: date)
            let pv = self.superview?.superview?.superview
            pv?.addSubview(bv!)
            bv!.frame = (pv?.bounds)!
            bv!.addSubview(v!)
            let f = bv!.bounds
            v?.frame = CGRect(x: 0, y: f.height * 0.2, width: f.width, height: f.height * 0.6)
            
            v!.confirmPressed = {
                date in
                
                self.setDate(nd: date)
                self.v!.removeFromSuperview()
                self.bv?.removeFromSuperview()
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
