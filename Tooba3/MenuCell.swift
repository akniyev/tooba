//
//  MenuCell.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 04/10/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class MenuCell : UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
}
