//
//  ImageInfo.swift
//  Tooba3
//
//  Created by Admin on 25/09/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import Foundation

class ImageInfo {
    var bigImageUrl = ""
    var smallImageUrl = ""
    var youtubeCode = ""
    
    func isVideo() -> Bool {
        return !youtubeCode.isEmpty
    }
    
    init() {
        
    }
    
    init(bigImageUrl : String, smallImageUrl : String) {
        self.bigImageUrl = bigImageUrl
        self.smallImageUrl = smallImageUrl
        self.youtubeCode = ""
    }
    
    init(youtubeCode: String) {
        self.youtubeCode = youtubeCode
        self.bigImageUrl = ""
        self.smallImageUrl = ""
    }
}
