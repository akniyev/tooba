//
//  ButtonTableViewCell.swift
//  Tooba3
//
//  Created by Admin on 19/09/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class ButtonTableViewCell: UITableViewCell, CellInfoReturner {
    @IBOutlet weak var button: UIButton!
    @IBAction func buttonTouch(_ sender: AnyObject) {
        if let cb = callback {
            cb(button)
        }
    }

    var callback : ((_ b : UIButton) -> ())? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getCellData() -> CellInfo {
        var l = ""

        if let _l = button.title(for: .normal) {
            l = _l
        }
        
        return CellInfo(type: .Button, label: l)
    }
}
