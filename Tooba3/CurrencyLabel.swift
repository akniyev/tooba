//
//  CurrencyLabel.swift
//  Tooba3
//
//  Created by Admin on 28/09/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class CurrencyLabel : UITextField, UITextFieldDelegate {
    unowned var p : Project = Project()
    private var value : Double = 0.0
    var payAction : (() -> ())?
    
    func setValue(x : Double) {
        value = round(x)
        if value < 0 {
            value = 0
        } else if value > max(0, p.goal - p.reached) {
            value = max(0, p.goal - p.reached)
        }
        
        self.text = p.currency.toString(value)
    }
    
    func getValue() -> Double {
        return value
    }
    
    override func endEditing(_ force: Bool) -> Bool {
        return true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
        self.keyboardType = .numberPad
        self.borderStyle = .roundedRect
        self.addDoneAndPayButtonsOnKeyboard()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if value > 0 {
            textField.text = "\(Int(value))"
        } else {
            textField.text = ""
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text == "" {
            textField.text = "0"
        }
        if let newValue = Double(textField.text!) {
            self.setValue(x: newValue)
        } else {
            self.setValue(x: value)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }
    
    func addDoneAndPayButtonsOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let smallSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        smallSpace.width = 10
        let done: UIBarButtonItem  = UIBarButtonItem(title: "HIDE".localized, style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        let pay: UIBarButtonItem  = UIBarButtonItem(title: "PAY".localized, style: UIBarButtonItem.Style.done, target: self, action: #selector(self.payButtonAction))
        pay.tintColor = UIColor(red: 0, green: 214.0 / 255.0, blue: 10.0 / 255.0, alpha: 1.0)
        
        var items = [UIBarButtonItem]()
        items.append(done)
        items.append(flexSpace)
        items.append(pay)

        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    
    @objc func payButtonAction() {
        self.payAction?()
    }
}
