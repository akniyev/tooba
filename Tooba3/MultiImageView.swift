//
//  ToobaMultiimageView.swift
//  CustomSliderExample
//
//  Created by Admin on 25/08/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import Kingfisher

class SliderURL {
    private var videoCode: String = ""
    private var imageUrl: String = ""
    
    init(videoCode: String) {
        self.setVideo(code: videoCode)
    }
    
    init(imageUrl: String) {
        self.setImage(imageUrl: imageUrl)
    }
    
    func setVideo(code: String) {
        self.videoCode = code
        self.imageUrl = ""
    }
    
    func setImage(imageUrl: String) {
        self.videoCode = ""
        self.imageUrl = imageUrl
    }
}

//class MyImageCache: SKImageCacheable {
//    func removeAllImages() {
//        
//    }
//    
//    
//    func imageForKey(_ key: String) -> UIImage? {
//        return ImageCache.default.retrieveImageInDiskCache(forKey: key)
//    }
//    
//    func setImage(_ image: UIImage, forKey key: String) {
//        ImageCache.default.store(image, forKey: key)
//    }
//    
//    func removeImageForKey(_ key: String) {
//        ImageCache.default.removeImage(forKey: key)
//    }
//}

class MultiImageView: UIView, UIScrollViewDelegate {
//    static var globalInstances : Set<MultiImageView> = []
    weak var scroll : MyScrollView!
    let paging = UIPageControl()
    weak var parentController : UIViewController? = nil
    
    var p : Project? = nil
    
    func stopVideos() {
        for mediaItem in imageViews {
            if mediaItem.mediaType == .Video {
                mediaItem.stopVideo()
            }
        }
    }
    
    func getPage() -> Int {
        var offset = scroll.contentOffset.x
        if offset < 0 { offset = 0 }
        if offset > scroll.bounds.width * CGFloat(imageCount - 1) {
            offset = scroll.bounds.width * CGFloat(imageCount - 1)
        }
        let page = Int(round(offset / scroll.bounds.width))

        return page
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let newPage = getPage()

        if paging.currentPage != newPage {
            let mediaItem = imageViews[paging.currentPage]
            mediaItem.stopVideo()
        }

        paging.currentPage = newPage
    }
    
    var imageCount = 1 {
        didSet {
            prepareScrollView()
        }
    }
    
    func setProject(p : Project) {
        imageCount = p.photos.count

        if (imageCount < 1) {
            return
        }

        self.p = p

        for i in 0 ... p.photos.count - 1 {
            if (!p.photos[i].isVideo()) {
                self.imageViews[i].setImage(url: p.photos[i].smallImageUrl)
            } else {
                self.imageViews[i].setVideo(code: p.photos[i].youtubeCode)
            }
        }
    }
    
    var imageViews : [ImageAndVideoView] = []

    func initialize() {
//        MultiImageView.globalInstances = MultiImageView.globalInstances.union([self])
        let scroll = MyScrollView()
        self.addSubview(scroll)
        self.scroll = scroll
        
        scroll.frame = self.bounds
        scroll.delegate = self

        self.addSubview(paging)
        let f = scroll.frame
        paging.frame = CGRect(x: 0, y: f.height - 20, width: f.width, height: 20)
        scroll.showsHorizontalScrollIndicator = false
        scroll.showsVerticalScrollIndicator = false

        scroll.touched = { [unowned self] in
            var images = [SKPhoto]()
            if let photos = self.p?.photos {
                let page = self.getPage()
                let currentPhoto = photos[page]
                if currentPhoto.isVideo() {
                    return
                }
                var antiPage = 0
                
                for (id, photoInfo) in photos.enumerated() {
                    if (!photoInfo.youtubeCode.isEmpty) {
                        if (id < page) {
                            antiPage += 1
                        }
                        continue
                    }
                    let photo =  SKPhoto.photoWithImageURL(photoInfo.bigImageUrl)
                    photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
                    images.append(photo)
                }
                
                // create PhotoBrowser Instance, and present.
                let browser = SKPhotoBrowser(photos: images)
                browser.initializePageIndex(page - antiPage)
                if let pc = self.parentController {
                    pc.present(browser, animated: true, completion: {})
                }
            }
        }

        prepareScrollView()
        self.paging.isUserInteractionEnabled = false
    }

    func prepareScrollView() {
        scroll.frame = self.bounds
        let f = scroll.bounds
        paging.frame = CGRect(x: 0, y: f.height - 40, width: f.width, height: 20)

        paging.numberOfPages = imageCount

        //for some reason when I set content height equal to bounds height, image have additional area on top
        //and I can scroll it down. this additional area has the height equals to 64
        scroll.contentSize = CGSize(width: scroll.bounds.width * CGFloat(imageCount), height: 0)
        var o = scroll.contentOffset
        o.y = 0
        scroll.setContentOffset(o, animated: false)
        scroll.isPagingEnabled = true
        scroll.bounces = false

        recreateImages()
    }
    
    func clearImages() {
        for i in imageViews {
            i.removeFromSuperview()
        }
        
        imageViews = []
    }

    func recreateImages() {
        if imageViews.count != imageCount {
            clearImages()

            if (imageCount > 0) {
                for i in 0 ... imageCount - 1 {
                    let f = CGRect(x: scroll.bounds.width * CGFloat(i), y: 0, width: scroll.bounds.width, height: scroll.bounds.height)

                    let img = ImageAndVideoView(frame: f)

                    imageViews.append(img)
                    scroll.addSubview(img)
                }
            }
        } else {
            if (imageCount > 0) {
                for i in 0 ... imageCount - 1 {
                    let f = CGRect(x: scroll.bounds.width * CGFloat(i), y: 0, width: scroll.bounds.width, height: scroll.bounds.height)
                    imageViews[i].frame = f
                }
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    deinit {
        print("DEINIT")
//        MultiImageView.globalInstances = MultiImageView.globalInstances.subtracting([self])
//        print("Set: \(MultiImageView.globalInstances.count)")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        prepareScrollView()
    }
}
