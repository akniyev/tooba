//
//  IntroductionSlider.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 30/03/2019.
//  Copyright © 2019 greenworld. All rights reserved.
//

import UIKit

class IntroductionSlider : UIViewController, UIScrollViewDelegate {
    var scroll : UIScrollView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
        initialize()
    }
    
    func initialize() {
        self.scroll = UIScrollView()
        self.view.addSubview(scroll)
    }
    
    @objc func buttonAction() {
        if (self.scroll.contentSize.width - self.scroll.contentOffset.x < self.scroll.bounds.width + 5) {
            self.dismiss(animated: true, completion: nil)
        } else {
            let offset = CGPoint(x: self.scroll.bounds.width + self.scroll.contentOffset.x, y: 0)
            self.scroll.setContentOffset(offset, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scroll.delegate = self
        self.scroll.frame = self.view.bounds
        self.scroll.backgroundColor = .white
        
        let images = [UIImage(named: "Slide1")!, UIImage(named: "Slide2")!, UIImage(named: "Slide3")!, UIImage(named: "Slide4")!]
        let width = self.scroll.bounds.width
        let height = self.scroll.bounds.height
        let scrollWidth = width * CGFloat(images.count)
        
        let buttonWidth = width * 0.6
        let buttonHeight = buttonWidth / 5.0
        let buttonX = (width - buttonWidth) / 2.0
        let buttonY = height - buttonHeight * 2
        
        self.scroll.contentSize = CGSize(width: scrollWidth, height: height)
        self.scroll.isPagingEnabled = true
        self.scroll.bounces = false
        
        for (id, image) in images.enumerated() {
            let imageView = UIImageView(image: image)
            let frame = CGRect(x: width * CGFloat(id), y: 0, width: width, height: height)
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.frame = frame
            self.scroll.addSubview(imageView)
            
            let button = UIButton()
            let buttonFrame = CGRect(x: buttonX + width * CGFloat(id), y: buttonY, width: buttonWidth, height: buttonHeight)
            button.frame = buttonFrame
            button.setBackgroundImage(UIImage(named: "ShareButtonBorder"), for: .normal)
            scroll.addSubview(button)
            button.setTitleColor(UIColor.lightGray, for: .normal)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            if (id == images.count - 1) {
                button.setTitle("CLOSE".localized, for: .normal)
            } else {
                button.setTitle("NEXT".localized, for: .normal)
            }
        }
    }
}
