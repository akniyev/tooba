//
//  FormTableViewCell.swift
//  Tooba3
//
//  Created by Admin on 19/09/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class EmailTableViewCell: UITableViewCell, CellInfoReturner {
    @IBOutlet weak var lblFieldName: UILabel!
    @IBOutlet weak var txtValue: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getCellData() -> CellInfo {
        var l = ""
        var ph = ""
        var v = ""
        
        if let _l = lblFieldName.text {
            l = _l
        }
        
        if let _v = txtValue.text {
            v = _v
        }
        
        if let _ph = txtValue.placeholder {
            ph = _ph
        }

        return CellInfo(type: .Email, label: l, value: v, placeholder: ph)
    }
}
