//
//  MyImageView.swift
//  Tooba3
//
//  Created by Admin on 09/10/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class MyImageView : UIImageView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        //print("Passing all touches to the next view (if any), in the view stack.")
        return false
    }
}
