//
//  FeedbackViewController2.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 13/01/2019.
//  Copyright © 2019 greenworld. All rights reserved.
//

import UIKit

class FeedbackViewController2 : UIViewController {
    @IBOutlet weak var containerView: UIView!
    
    weak var feedbackForm: FeedbackViewController?
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBAction func btnSendTouched(_ sender: Any) {
        feedbackForm?.SendFeedback()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSend.setTitle("SEND_FEEDBACK".localized.uppercased(), for: .normal)
        
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(keyboardDidShow(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(keyboardDidHide(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func keyboardDidShow (_ notification: NSNotification) {
        var keyboardHeight : CGFloat = 0.0

        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                keyboardHeight = keyboardSize.height
            }
        }
        self.bottomConstraint.constant = keyboardHeight
    }
    
    @objc func keyboardDidHide (_ notification: NSNotification) {
        self.bottomConstraint.constant = 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embed" {
            self.feedbackForm = segue.destination as? FeedbackViewController
            self.feedbackForm?.feedbackVC2 = self
        }
    }
}
