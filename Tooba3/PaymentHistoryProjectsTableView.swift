//
//  PaymentHistoryProjectsTableView.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 15/01/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit
import Kingfisher

class PaymentHistoryProjectsTableView: UITableView, UITableViewDelegate, UITableViewDataSource {
    var projects : [Project] = []
    let customRowHeight : CGFloat = UIScreen.main.bounds.width * 370.0 / 1080.0
    var listType : ProjectListType = ProjectListType.active
    var myRefreshControl: UIRefreshControl? = nil
    
    var itemsOnPage = 20
    var endPage = -1
    
    var backView : BackgroundView? = nil
    
    var pagesLoading : Set<Int> = []
    var pagesLoaded : Set<Int> = []
    
    @objc func pullRefreshPage() {
        pagesLoading.removeAll()
        pagesLoaded.removeAll()
    }
    
    func loadCurrentPages(forced: Bool = false) {
        let pageHeight = Double(itemsOnPage) * Double(customRowHeight)
        let currentOffset = self.contentOffset.y
        
        var p1 = 0
        var p2 = 1
        
        p1 = Int((Double(currentOffset) + pageHeight / 2) / pageHeight)
        p2 = p1 + 1
        
        if forced {
            pagesLoaded.remove(p1)
            pagesLoaded.remove(p2)
        }
        
        getPage(pageNumber: p1)
        getPage(pageNumber: p2)
    }
    
    func getPage(pageNumber : Int) {
        if pagesLoaded.contains(pageNumber) {
            return
        }
        
        if pageNumber < 1 {
            return
        }
        
        if endPage != -1 && pageNumber > endPage {
            return
        }
        
        if !pagesLoading.contains(pageNumber) {
            pagesLoading.insert(pageNumber)
            if self.myRefreshControl?.isRefreshing == false && self.projects.count == 0 {
                self.showReloadView()
            }
            Server.GetDonationHistory(itemsOnPage : itemsOnPage, pageNumber : pageNumber,
                                  YES: { [unowned self] projects in
                                    self.hideReloadView()
                                    self.myRefreshControl?.endRefreshing()
                                    self.backView?.label.text = ""
                                    
                                    if self.endPage == -1 && projects.count < self.itemsOnPage {
                                        self.endPage = pageNumber
                                    }
                                    
                                    let itemsCount = (pageNumber - 1) * self.itemsOnPage + projects.count
                                    
                                    DispatchQueue.main.async { [unowned self] in
                                        if pageNumber == self.endPage {
                                            if projects.count == self.itemsOnPage {
                                                self.endPage = -1
                                            } else if projects.count == 0 {
                                                self.endPage = -1
                                                while self.projects.count > itemsCount {
                                                    self.projects.removeLast()
                                                }
                                            } else {
                                                while self.projects.count > itemsCount {
                                                    self.projects.removeLast()
                                                }
                                            }
                                        }
                                        
                                        while self.projects.count < itemsCount {
                                            self.projects.append(Project())
                                        }
                                        
                                        let startIndex = (pageNumber - 1) * self.itemsOnPage
                                        
                                        if projects.count > 0 {
                                            for i in 0 ... projects.count - 1 {
                                                self.projects[startIndex + i] = projects[i]
                                            }
                                        }
                                        
                                        self.pagesLoaded.insert(pageNumber)
                                        
                                        if self.projects.count == 0 {
                                            self.backgroundView = self.backView
                                            self.backView?.label.text = "NO_DONATIONS".localized
                                        } else {
                                            self.backgroundView = nil
                                        }
                                        
                                        self.reloadData()
                                    }
                                    
                                    self.pagesLoading.remove(pageNumber)
                }, NO: { [unowned self] et, msg in
                    self.hideReloadView()
                    self.backView?.label.text = "CONNECTION_ERROR".localized
                    self.myRefreshControl?.endRefreshing()
                    self.pagesLoading.remove(pageNumber)
            })
        }
    }
    
    var RowSelected : ((Project, Int) -> ())? = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        let nib = UINib(nibName: "PaymentHistoryProjectCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "PaymentHistoryProjectCell")
        self.dataSource = self
        self.delegate = self
        self.allowsSelection = true
        self.separatorStyle = .none
        
        backView = BackgroundView(frame: self.bounds)
//        self.backgroundView = backView
        backView?.label.text = ""
        
        self.myRefreshControl = UIRefreshControl();
        self.addSubview(self.myRefreshControl!);
        self.myRefreshControl?.addTarget(self, action: #selector(pullRefreshPage), for: .valueChanged)
        
        self.sectionHeaderHeight = 20
        self.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: 20))
    }
    
    func setProjects(ps : [Project]) {
        projects = ps
        self.reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        loadCurrentPages()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if let rowSelected = RowSelected {
            rowSelected(projects[indexPath.row], indexPath.row)
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dequeueReusableCell(withIdentifier: "PaymentHistoryProjectCell") as! PaymentHistoryProjectCell
        let p = projects[indexPath.row]
        
        cell.lblTag.text = "@" + p.tag.trimmingCharacters(in: ["@"])
        
        cell.setLikeCount(likeCount: p.donatedCount)
        cell.lblDescription.text = p.short_description
        cell.lblDonated.text = p.currency.toString(p.donatedByMe)
        if p.photos.count > 0 {
            cell.img.kf.setImage(with: URL(string: p.photos[0].smallImageUrl))
        }
        cell.coloredLine.backgroundColor = ProjectListViewController.ColorForTitleView(row: indexPath.row)
        cell.finishedDateSection.isHidden = true
        cell.labelForDonated.text = "DONATED".localized
        cell.checkmarkView.isHidden = true
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let c = cell as! PaymentHistoryProjectCell
        c.img.kf.cancelDownloadTask()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return customRowHeight
    }
    
}
