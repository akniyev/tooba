//
// Created by Admin on 22/01/2017.
// Copyright (c) 2017 greenworld. All rights reserved.
//

import Foundation

enum PaymentSystem {
    case WebMoney
    case Paypal
    case Worldpay

    func code() -> String {
        switch self {
        case .Worldpay: return "worldpay"
        case .WebMoney: return "webmoney"
        case .Paypal: return "paypal"
        default: return ""
        }
    }

    func toString() -> String {
        switch self {
        case .Worldpay: return "WorldPay"
        case .WebMoney: return "WebMoney"
        case .Paypal: return "PayPal"
        default: return ""
        }
    }
}