//
//  PaymentHistoryItem.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 27/10/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import Foundation

class PaymentHistoryItem {
    var id              : Int
    var projectId       : Int
    var projectTitle    : String
    var amount          : Double
    var currencyId      : Int
    var date            : Date
    var paymentInfo     : String
    
    init(id: Int, projectId: Int, projectTitle: String, amount: Double, currencyId: Int, paymentInfo: String, date: Date) {
        self.id             = id
        self.projectId      = projectId
        self.projectTitle   = projectTitle
        self.amount         = amount
        self.currencyId     = currencyId
        self.paymentInfo    = paymentInfo
        self.date           = date
    }
}
