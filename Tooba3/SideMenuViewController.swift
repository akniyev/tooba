//
//  SideMenuViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 16/12/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuViewController : UISideMenuNavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SideMenuManager.default.menuPresentMode = .menuSlideIn
    }
    
    
}
