//
//  BackgroundView.swift
//  Tooba3
//
//  Created by Admin on 03/12/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class BackgroundView : UIView {
    @IBOutlet weak var label: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInitialization()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInitialization()
    }
    
    func commonInitialization() {
        if let view = Bundle.main.loadNibNamed("BackgroundView", owner: self, options: nil)?.first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
    }
}
