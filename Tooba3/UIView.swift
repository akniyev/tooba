//
//  UIView.swift
//  Tooba3
//
//  Created by Hasan on 31/07/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import UIKit

extension UIView {
    
    func showReloadView() {
        ShowReloadViewWithColors(backgroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 120/255), indicatorColor: UIColor.white)
    }
    
    func ShowWhiteReloadView() {
        ShowReloadViewWithColors(backgroundColor: UIColor(red: 1, green: 1, blue: 1, alpha: 1), indicatorColor: UIColor.black)
    }
    
    func ShowReloadViewWithColors(backgroundColor: UIColor, indicatorColor: UIColor) {
        if let tv = self as? UITableView {
            tv.isScrollEnabled = false
        }
        let reloadView: IndicatorView = Bundle.main.loadNibNamed("IndicatorView", owner: nil, options: nil)?.first! as! IndicatorView
        reloadView.backgroundColor = backgroundColor
        reloadView.indicator.color = indicatorColor
        reloadView.frame = self.bounds
        self.addSubview(reloadView)
    }
    
    func hideReloadView() {
        if let tv = self as? UITableView {
            tv.isScrollEnabled = true
        }
        for v in self.subviews.filter({$0 is IndicatorView}) {
            v.removeFromSuperview()
        }
    }
}

extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
