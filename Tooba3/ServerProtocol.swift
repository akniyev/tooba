//
//  ServerProtocol.swift
//  Tooba3
//
//  Created by Admin on 18/09/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import Foundation

enum ProjectListType {
    case active
    case Favorites
    case finished
}

protocol ServerProtocol {
    static func signUp (email: String, password: String, YES: (() -> ())?, NO: ((_ message: String) -> ())?)
    static func signIn (email: String, password: String, YES: ((Credentials) -> ())?, NO: ((_ message: String) -> ())?)
    static func refreshToken (YES : (() -> ())?, NO : (() -> ())?)
    static func getProjectList(listType : ProjectListType, itemsOnPage : Int, pageNumber : Int, YES: ((_ projects: [Project]) -> ())?, NO: ((_ message: String) -> ())?)
    static func resetPassword(email: String, YES: (() -> ())?, ERROR: ((String)->())?)
    static func addProjectToFavorites(id: Int, YES: (() -> ())?, NO : ((String) -> ())?)
    static func removeProjectFromFavorites(id: Int, YES: ((() -> ()))?, NO : ((String) -> ())?)
    static func signOut()
    static func resendConfirmationEmail(YES: (() -> ())?, NO: ((String) -> ())?)
}
