//
//  Project.swift
//  Tooba
//
//  Created by Admin on 26/07/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class Project {
    var id : Int = -1
    var name = ""
    var tag = ""
    var description = ""
    var short_description = ""
    var photos: [ImageInfo] = []
    var goal: Double = 0.0
    var reached: Double = 0.0
    var currency: Currency = .ruble
    var likeCount : Int = 0
    var isLiked : Bool = false
    var donatedByMe : Double = 0
    var finishedDate : Date? = Date()
    var donatedCount : Int = -1
    var hasSameTagFinishedProjects: Bool = false
    var fundName = ""
}
