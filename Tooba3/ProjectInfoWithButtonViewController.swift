//
//  ProjectInfoWithButtonViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 16/02/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class ProjectInfoWithButtonViewController : UIViewController {
    static func storyboardInstance() -> ProjectInfoWithButtonViewController? {
        let storyboard = UIStoryboard(name: "ProjectInfo", bundle: nil)
        return storyboard.instantiateInitialViewController() as? ProjectInfoWithButtonViewController
    }
    
    @IBOutlet weak var button_Finished: UIButton!
    public var projectInfo: Project?
    public var projectIdToFetch: Int?
    public var projectTagToFetch: String?
    
    weak var subcontroller : ProjectInfoScrollableViewController?
    
    @IBOutlet weak var scrollableProjectInfo: UIView!
    @IBAction func finishedProjectsTouched(_ sender: Any) {
        if let sc = self.subcontroller {
            if let vc = FinishedProjectsViewController.storyboardInstance() {
                vc.tagToLoad = sc.projectInfo.tag
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button_Finished.setTitle("FINISHED_PROJECTS".localized, for: .normal)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ProjectInfoScrollableViewController {
            vc.projectInfo = self.projectInfo
            vc.projectIdToFetch = self.projectIdToFetch
            vc.projectTagToFetch = self.projectTagToFetch
            vc.enclosingViewController = self
            self.subcontroller = vc
        }
    }
}
