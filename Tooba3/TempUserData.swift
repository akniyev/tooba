//
// Created by Hasan Akniyev on 18/09/2017.
// Copyright (c) 2017 greenworld. All rights reserved.
//

import Foundation

class TempUserData {
    var username = ""
    var password = ""
    var active = false

    init(username: String, password: String, active: Bool = true) {
        self.username = username
        self.password = password
        self.active = active
    }

    init() {

    }
}
