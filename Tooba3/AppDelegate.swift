//
//  AppDelegate.swift
//  Tooba3
//
//  Created by Admin on 17/09/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit
import CoreData
//import FacebookCore
//import GoogleSignIn
import UserNotifications
import SKPhotoBrowser
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if application.applicationState == .inactive || application.applicationState == .background {
            if let pId = (userInfo["projectId"] as? NSString)?.intValue {
                let projectId = Int.init(pId)
                print("OPEN \(projectId)")
            
                let rootViewController = self.window!.rootViewController as! UINavigationController
                
                if rootViewController.viewControllers.count == 1 {
                    let loginView = rootViewController.viewControllers[0] as! ProjectListViewController
                    let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
                    vc.projectIdToFetch = projectId
                    rootViewController.pushViewController(vc, animated: true)
                } else if rootViewController.viewControllers.count > 1 {
                    let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
                    vc.projectIdToFetch = projectId
                    rootViewController.pushViewController(vc, animated: true)
                }
            }
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
//        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
//        GIDSignIn.sharedInstance().clientID = "613860394816-bjaue3ohij69b23a7o21lb2oq494r6te.apps.googleusercontent.com"
//        GIDSignIn.sharedInstance().uiDelegate = self

        initializeNotificationServices()
        
//        SKCache.sharedCache.imageCache = MyImageCache()
        
        application.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        
        return true
    }
    
    var applicationStateString: String {
        if UIApplication.shared.applicationState == .active {
            return "active"
        } else if UIApplication.shared.applicationState == .background {
            return "background"
        }else {
            return "inactive"
        }
    }
    
    func requestNotificationAuthorization(application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self as! UNUserNotificationCenterDelegate
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }
    
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        print("URL URL URL")
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                let cs = url.pathComponents
                print(cs)
                if (cs.count == 4 && cs[0] == "/" && cs[1] == "iosapp" && cs[2] == "projects") {
                    if let projectId = Int(cs[3]) {
                        openProjectWith(id: projectId)
                    }
                } else if (cs.count == 3 && cs[0] == "/" && (cs[1] == "m" || cs[1] == "ios")) {
                    let tag = cs[2]
                    
                    openProjectWith(tag: tag)
                }
            }
        }
        return true
    }
    
    func openProjectWith(tag: String) {
        let rootViewController = self.window!.rootViewController as! UINavigationController
        
        if rootViewController.viewControllers.count == 1 {
            let loginView = rootViewController.viewControllers[0] as! ProjectListViewController
            
            if (loginView.loaded) {
                let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
                vc.projectTagToFetch = tag
                rootViewController.pushViewController(vc, animated: true)
            } else {
                loginView.projectTagToFetch = tag
            }
        } else if rootViewController.viewControllers.count > 1 {
            let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
            vc.projectTagToFetch = tag
            rootViewController.pushViewController(vc, animated: true)
        }
    }
    
    func openProjectWith(id : Int) {
        let rootViewController = self.window!.rootViewController as! UINavigationController
        
        if rootViewController.viewControllers.count == 1 {
            let loginView = rootViewController.viewControllers[0] as! ProjectListViewController
            
            if (loginView.loaded) {
                let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
                vc.projectIdToFetch = id
                rootViewController.pushViewController(vc, animated: true)
            } else {
                loginView.projectIdToFetch = id
            }
        } else if rootViewController.viewControllers.count > 1 {
            let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
            vc.projectIdToFetch = id
            rootViewController.pushViewController(vc, animated: true)
        }
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if(url.scheme!.isEqual("toobaiosurlscheme")) {
            let components = url.pathComponents
            if components.count == 3 {
                if components[0] == "/" && components[1] == "projects" {
                    if let projectId = Int(components[2]) {
                        openProjectWith(id: projectId)
                        return true
                    }
                }
            }
            return true
        } else {
//            var options: [String: AnyObject] = [UIApplicationOpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject,
//                                                UIApplicationOpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
//            return GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: sourceApplication, annotation: annotation)
            return true
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
//        AppEventsLogger.activate(application)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "coreDataTestForPreOS")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // iOS 9 and below
    lazy var applicationDocumentsDirectory: URL = {
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "coreDataTestForPreOS", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.0, *) {
            
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } else {
                // iOS 9.0 and below - however you were previously handling it
            if managedObjectContext.hasChanges {
                do {
                    try managedObjectContext.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                    abort()
                }
            }
        }
    }

//    Push notification for ios10+
//    func registerForPushNotifications() {
//        if #available(iOS 10.0, *) {
//            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
//                print("Permission granted: \(granted)")
//
//                guard granted else { return }
//                self.getNotificationSettings()
//            }
//        } else {
//
//        }
//    }
//
//    func getNotificationSettings() {
//        if #available(iOS 10.0, *) {
//            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
//                print("Notification settings: \(settings)")
//                guard settings.authorizationStatus == .authorized else { return }
//                DispatchQueue.main.async {
//                    UIApplication.shared.registerForRemoteNotifications()
//                }
//            }
//        } else {
//            // Fallback on earlier versions
//        }
//    }
//
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let tokenParts = deviceToken.map { data -> String in
//            return String(format: "%02.2hhx", data)
//        }
//
//        let token = tokenParts.joined()
//        print("Device Token: \(token)")
//    }
//
//    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print("Failed to register: \(error)")
//    }
    
//    Push notification for iOS9
    func initializeNotificationServices() -> Void {
        let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        
        // This is an asynchronous method to retrieve a Device Token
        // Callbacks are in AppDelegate.swift
        // Success = didRegisterForRemoteNotificationsWithDeviceToken
        // Fail = didFailToRegisterForRemoteNotificationsWithError
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenStr = convertDeviceTokenToString(deviceToken as NSData)
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            Server.SendFirebaseToken(token: refreshedToken, YES: {
                print("YES")
            }, NO: {
                print("NO")
            })
        }
        print("Device token: \(deviceTokenStr)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Device token for push notifications: FAIL -- ")
        print(error.localizedDescription)
    }
    
    private func convertDeviceTokenToString(_ deviceToken: NSData) -> String {
        //  Convert binary Device Token to a String (and remove the <,> and white space charaters).
        var deviceTokenStr = deviceToken.description.replacingOccurrences(of: ">", with: "")
        deviceTokenStr = deviceToken.description.replacingOccurrences(of: "<", with: "")
        deviceTokenStr = deviceToken.description.replacingOccurrences(of: " ", with: "")
        
        deviceTokenStr = deviceTokenStr.uppercased()
        return deviceTokenStr
    }
}

