//
//  CustomTextField.swift
//  Tooba3
//
//  Created by Admin on 05/11/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class CustomTextField : UITextField {
    
    @IBInspectable
    var iconName : String = "" {
        didSet {
            self.leftViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            let imageName = iconName
            let image = UIImage(named: imageName)
            print(iconName)
            imageView.image = image
            imageView.contentMode = .center
            self.leftView = imageView
        }
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 25, y: bounds.origin.y, width: bounds.width - 25, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 25, y: bounds.origin.y, width: bounds.width - 25, height: bounds.height)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.leftViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let imageName = iconName
        let image = UIImage(named: imageName)
        print(iconName)
        imageView.image = image
        self.leftView = imageView
    }
    
    override func draw(_ rect: CGRect) {
        
        let startingPoint   = CGPoint(x: rect.minX, y: rect.maxY)
        let endingPoint     = CGPoint(x: rect.maxX, y: rect.maxY)
        
        let path = UIBezierPath()
        
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 2.0
        
        tintColor.setStroke()
        
        path.stroke()
    }
}
