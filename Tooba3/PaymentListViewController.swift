//
//  PaymentListViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 26/10/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import UIKit

class PaymentListViewController : UITableViewController {
    static func storyboardInstance() -> PaymentListViewController? {
        let storyboard = UIStoryboard(name: "Payment", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PaymentListViewController") as? PaymentListViewController
    }
    
    var paymentHistory: [PaymentHistoryItem] = []
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.paymentHistory.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentHistoryCell") as! PaymentHistoryCell
        
        let ph = self.paymentHistory[indexPath.row]
        cell.lblProjectTitle.text = "@\(ph.projectTitle)"
        cell.lblAmount.text = "\(ph.amount)₽"
        cell.lblCardInfo.text = ph.paymentInfo
        cell.lblDate.text = DateFormatter.localizedString(from: ph.date, dateStyle: .medium, timeStyle: .short)
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.ShowWhiteReloadView()
        Server.YandexKassaPaymentHistory(SUCCESS: { [unowned self] history in
            self.view.hideReloadView()
            self.paymentHistory = history
            self.tableView.reloadData()
        }) {
            self.view.hideReloadView()
        }
    }
    
   
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let projectId = self.paymentHistory[indexPath.row].projectId
        if let vc = ProjectInfoWithButtonViewController.storyboardInstance() {
            vc.projectIdToFetch = projectId
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return false
    }
}

class PaymentHistoryCell : UITableViewCell {
    @IBOutlet weak var lblProjectTitle: UILabel!
    @IBOutlet weak var lblCardInfo: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}
