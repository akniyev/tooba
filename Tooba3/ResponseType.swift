//
// Created by Admin on 04/12/2016.
// Copyright (c) 2016 greenworld. All rights reserved.
//

import Foundation

enum ResponseType {
    case Json
    case Data
}