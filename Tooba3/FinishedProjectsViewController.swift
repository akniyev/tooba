//
//  FinishedProjectsViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 16/02/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class FinishedProjectsViewController : UIViewController, UIGestureRecognizerDelegate {
    static func storyboardInstance() -> FinishedProjectsViewController? {
        let storyboard = UIStoryboard(name: "ProjectInfo", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "Finished") as? FinishedProjectsViewController
    }
    
    @IBOutlet weak var tableView: FinishedProjectsTableView!
    var tagToLoad: String = ""
    var backView : BackgroundView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tagToLoad = self.tagToLoad
        
        self.backView = BackgroundView(frame: self.view.bounds);
        self.view.addSubview(self.backView!);
        
        self.backView?.label.text = "PRESS_TO_RELOAD".localized
        self.backView?.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.reloadTapped))
        tap.delegate = self
        self.backView?.addGestureRecognizer(tap)
        self.navigationItem.title = "FINISHED".localized
        
        tableView.RowSelected = { [unowned self] (p, i) in
            let vc = ProjectInfoScrollableViewController.storyboardInstance()!
            vc.projectInfo = p
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.tableView.backgroundView = nil
        self.tableView.backgroundColor = UIColor(red: 238/255.0, green: 242/255.0, blue: 244/255.0, alpha: 1)
    }
    
    @objc func reloadTapped() {
        print("RELOAD")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.parent?.title = "PROJECT_LIST_PROJECTS_TITLE".localized
        
        if Db.isAuthorized() {
            self.tableView.loadCurrentPages(forced: true)
        }
    }
}

