//
//  ToobaSliderThumbLayer.swift
//  CustomSliderExample
//
//  Created by Admin on 24/08/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit
import QuartzCore

class ToobaSliderThumbLayer: CALayer {
    weak var toobaSlider : ToobaSlider?
    
    override func draw(in ctx: CGContext) {
        if let slider = toobaSlider {
            if slider.pressed {
                let path = UIBezierPath(ovalIn: slider.thumbFrame())
                ctx.addPath(path.cgPath)
                ctx.setFillColor(slider.trackFillColor2pressed.cgColor)
                ctx.addPath(path.cgPath)
                ctx.clip()
                let divider : CGFloat = 1.5
                let currentContext = ctx
                currentContext.saveGState();
                let colorSpace = CGColorSpaceCreateDeviceRGB()
                
                var colorComponents : [CGFloat] = [
                    1.0 / divider, 0.03 / divider, 0.0, 1.0,
                    0.996 / divider, 0.714 / divider, 0.0, 1.0,
                    0.678 / divider, 0.863 / divider, 0.0, 1.0,
                    0.424 / divider, 0.902 / divider, 0.0, 1.0,
                    0.325 / divider, 0.82 / divider, 0.0, 1.0
                ]
                var locations:[CGFloat] = [0.0, 0.33, 0.75, 0.9, 1.0]
                let gradient = CGGradient(colorSpace: colorSpace,colorComponents: &colorComponents,locations: &locations,count: 5)
                
                let startPoint = CGPoint(x: 0, y: self.bounds.height)
                let endPoint = CGPoint(x: self.bounds.width, y: self.bounds.height)
                currentContext.drawLinearGradient(gradient!,start: startPoint,end: endPoint, options: CGGradientDrawingOptions(rawValue: 0))
                currentContext.restoreGState();

            } else {
                let path = UIBezierPath(ovalIn: slider.thumbFrame())
                ctx.addPath(path.cgPath)
                ctx.setFillColor(slider.trackFillColor2.cgColor)
                ctx.addPath(path.cgPath)

                ctx.clip()
                let currentContext = ctx
                currentContext.saveGState();
                let colorSpace = CGColorSpaceCreateDeviceRGB()
                
                var colorComponents : [CGFloat] = [
                    1.0, 0.03, 0.0, 1.0,
                    0.996, 0.714, 0.0, 1.0,
                    0.678, 0.863, 0.0, 1.0,
                    0.424, 0.902, 0.0, 1.0,
                    0.325, 0.82, 0.0, 1.0
                ]
                var locations:[CGFloat] = [0.0, 0.33, 0.75, 0.9, 1.0]
                let gradient = CGGradient(colorSpace: colorSpace,colorComponents: &colorComponents,locations: &locations,count: 5)
                
                let startPoint = CGPoint(x: 0, y: self.bounds.height)
                let endPoint = CGPoint(x: self.bounds.width, y: self.bounds.height)
                currentContext.drawLinearGradient(gradient!,start: startPoint,end: endPoint, options: CGGradientDrawingOptions(rawValue: 0))
                currentContext.restoreGState();
            }
        }
    }
}
