//
// Created by Admin on 04/12/2016.
// Copyright (c) 2016 greenworld. All rights reserved.
//

import Foundation

enum ContentType {
    case applicationFormUrlEncoded
    case applicationJson

    func toHttpHeaderValue() -> String {
        switch self {
        case .applicationFormUrlEncoded:
            return "application/x-www-form-urlencoded"
        case .applicationJson:
            return "application/json"
        }
    }
}
