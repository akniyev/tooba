//
//  ProjectInfoScrollableViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 11/06/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import UIKit

final class ProjectInfoScrollableViewController: UITableViewController {
    
    //MARK: Outlets
    @IBOutlet private weak var imageSlider: MultiImageView!
    @IBOutlet private weak var goalValueLabel: UILabel!
    @IBOutlet private weak var goalTitleLabel: UILabel!
    @IBOutlet private weak var reachedValueLabel: UILabel!
    @IBOutlet private weak var reachedTitleLabel: UILabel!
    @IBOutlet private weak var donateValueLabel: CurrencyLabel!
    @IBOutlet private weak var donateTitleLabel: UILabel!
    @IBOutlet private weak var payButton: UIButton!
    @IBOutlet private weak var shareButton: UIButton!
    @IBOutlet private weak var likeCountLabel: UILabel!
    @IBOutlet private weak var tagsLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var contentViewWithInsets: UIView!
    @IBOutlet private weak var gatheredForFinishedProjectLabel: UILabel!
    @IBOutlet private weak var panelForFinishedStackView: UIStackView!
    @IBOutlet private weak var panelForUnfinishedStackView: UIStackView!
    @IBOutlet private weak var reachedForFinishedLabel: UILabel!
    @IBOutlet private weak var finishedLabel: UILabel!
    @IBOutlet private weak var fondNameLabel: UILabel!
    
    //MARK: - Private properties
    private var lastPos: CGFloat = 0
    private var loadedOneTime = false
    private var reloaded = false
    
    //MARK: - Properties
    weak var projectInfo: Project!
    var projectIdToFetch: Int?
    var projectTagToFetch: String?
    weak var enclosingViewController: ProjectInfoWithButtonViewController?
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !reloaded {
            reloaded = true
            tableView.reloadData()
        }
        
        contentViewWithInsets.roundCorners(corners: [.allCorners], radius: 8)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.projectInfo != nil {
            let label = UILabel()
            label.font = self.descriptionLabel.font
            label.text = self.projectInfo.description
            label.numberOfLines = 0
            let wigth = UIScreen.main.bounds.width * (1 - 0.046296 * 2) - 16
            let rect = label.sizeThatFits(CGSize(width: wigth, height: 10000))
            let screenWidth = UIScreen.main.bounds.width
            var result = self.descriptionLabel.frame.origin.y + rect.height + (screenWidth <= 330 ? 10 : (wigth / 300 * 50))
            result = max(self.lastPos, result)
            self.lastPos = result
            return result
        } else {
            return 1000
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.loadedOneTime {
            fetchProjectInfoWithSilentReload()
        } else {
            if let projectID = self.projectIdToFetch {
                self.view.ShowWhiteReloadView()
                fetchProjectInfo(with: projectID)
            } else if let projectTag = self.projectTagToFetch {
                self.view.ShowWhiteReloadView()
                fetchProjectInfo(with: projectTag)
            } else {
                self.loadedOneTime = true
                setProjectInfo()
                fetchProjectInfo(with: self.projectInfo.id)
            }
        }
    }
    
    deinit {
        print("DEINIT")
    }
}

//MARK: - Setup
extension ProjectInfoScrollableViewController {
    
    private func setup() {
        setupLabels()
        enclosingViewController?.button_Finished.isHidden = true
        setupTableView()
        donateValueLabelSetup()
    }
    
    private func setupLabels() {
        reachedForFinishedLabel.text = "LABEL_REACHED".localized
        goalTitleLabel.text = "LABEL_GOAL".localized
        reachedTitleLabel.text = "LABEL_REACHED".localized
        donateTitleLabel.text = "LABEL_HELP".localized
        finishedLabel.text = "TEXTFINISHED".localized
    }
    
    private func setupTableView() {
        self.tableView.backgroundView = nil
        self.tableView.backgroundColor = UIColor(red: 238/255, green: 242/255, blue: 244/255, alpha: 1.0)
        self.tableView.layoutIfNeeded()
    }
    
    private func donateValueLabelSetup() {
        self.donateValueLabel.payAction = { [weak self] in
            guard let self = self else { return }
            self.donateValueLabel.resignFirstResponder()
            let amount = self.donateValueLabel.getValue()
            if amount >= ProjectListViewController.silentPaymentLimit {
                Alerts.ShowActionSheet(sender: self,
                                       title: "DO_YOU_REALLY_WANT_TO_PAY".localized + " \(self.projectInfo.currency.toString(amount))?", items: ["YES".localized, "NO".localized],
                                       callback: { [weak self] choice in
                                        if choice == 0 {
                                            self?.showSelector()
                                        }
                    }, completion: nil)
            } else {
                self.showSelector()
            }
        }
    }
    
    /// Get information about a project by the id
    private func fetchProjectInfo(with id: Int) {
        Server.getProjectInfo(projectId: id,
                              successCompletion: { [weak self] projectInfo in
                                guard let self = self else { return }
                                self.projectInfoResponse(projectInfo)
            }, onErrorCompletion: { [weak self] in
                guard let self = self else { return }
                self.projectInfoResponseError()
        })
    }
    
    /// Get information about a project by the tag
    private func fetchProjectInfo(with tag: String) {
        Server.getProjectInfo(projectTag: tag, successCompletion: { [weak self] projectInfo in
            guard let self = self else { return }
            self.projectInfoResponse(projectInfo)
            }, NO: { [weak self] in
                guard let self = self else { return }
                self.projectInfoResponseError()
        })
    }
    
    /// Response handling of fetchProjectInfo() request
    private func projectInfoResponse(_ projectInfo: Project) {
        self.projectInfo = projectInfo
        self.setProjectInfo()
        self.reloaded = false
        self.tableView.reloadData()
        self.tableView.layoutSubviews()
        self.loadedOneTime = true
        self.view.hideReloadView()
    }
    
    /// Error handling of fetchProjectInfo() request
    private func projectInfoResponseError() {
        self.view.hideReloadView()
        guard let navigationController = navigationController else { return }
        navigationController.popViewController(animated: true)
        Alerts.showErrorAlertWithOK(sender: navigationController,
                                    title: "ERROR".localized,
                                    message: "CANT_OPEN_PROJECT_FROM_URL".localized,
                                    completion: nil)
    }
    
    func fetchProjectInfoWithSilentReload() {
        let projectID = self.projectInfo.id
        Server.getProjectInfo(projectId: projectID, successCompletion: { [weak self] projectInfo in
            guard let self = self else { return }
            self.projectInfo = projectInfo
            self.setProjectInfo()
            self.reloaded = false
            self.tableView.layoutSubviews()
            }, onErrorCompletion: nil)
    }
    
    func showSelector() {
        if !payButton.isEnabled {
            return
        }
        self.view.showReloadView()
        Server.yandexKassaLinkedBankCards(successCompletion: { [weak self] bankCardItems in
            guard let self = self else { return }
            self.view.hideReloadView()
            
            if bankCardItems.count == 0 {
                self.payAction()
                return
            }
            self.payAction(bankCardItems[0].cardMask)
            }, onErrorCompletion: { [weak self] in
                guard let self = self else { return }
                self.view.hideReloadView()
                Alerts.showErrorAlertWithOK(sender: self, title: "ERROR".localized, message: "CANT_MAKE_A_PAYMENT".localized, completion: nil)
        })
    }
    
    func payAction(_ cardMask: String = "") {
        donateValueLabel.resignFirstResponder()
        payButton.isEnabled = false
        self.view.showReloadView()
        
        Server.yandexKassaPayment(projectId: projectInfo.id, amount: donateValueLabel.getValue(), cardMask: cardMask, successCompletion: { [weak self] urlString in
            guard let self = self else { return }
            self.view.hideReloadView()
            
            if urlString == "Ok" {
                let vc = MotivationViewController.storyboardInstance()
                vc.projectInfo = self.projectInfo
                self.present(vc, animated: true, completion: { [weak self] in
                    guard let self = self else { return }
                    self.fetchProjectInfoWithSilentReload()
                })
            } else {
                guard let url = URL(string: urlString) else { return }
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
            self.payButton.isEnabled = true
            }, onErrorCompletion: { [weak self] in
                guard let self = self else { return }
                self.view.hideReloadView()
                Alerts.showErrorAlertWithOK(sender: self, title: "Ошибка", message: "CANT_MAKE_A_PAYMENT".localized, completion: nil)
                self.payButton.isEnabled = true
        })
    }
    
    fileprivate func setProjectInfo() {
        if projectInfo.reached >= projectInfo.goal {
            donateValueLabel.isEnabled = false
            donateValueLabel.textColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
            payButton.isEnabled = false
        }
        
        enclosingViewController?.button_Finished.isHidden = !projectInfo.hasSameTagFinishedProjects
        
        goalValueLabel.text = projectInfo.currency.toString(projectInfo.goal)
        reachedValueLabel.text = projectInfo.currency.toString(projectInfo.reached)
        gatheredForFinishedProjectLabel.text = projectInfo.currency.toString(projectInfo.reached)
        donateValueLabel.p = projectInfo
        donateValueLabel.setValue(x: 0)
        likeCountLabel.text = "\(projectInfo.donatedCount)"
        donateValueLabel.borderStyle = .none
        fondNameLabel.text = projectInfo.fundName
        tagsLabel.text = "@\(projectInfo.tag)"
        
        enclosingViewController?.title = "@\(projectInfo.tag.uppercased())"
        self.title = "@\(projectInfo.tag.uppercased())"
        
        let reached = projectInfo.reached >= projectInfo.goal
        panelForFinishedStackView.isHidden = !reached
        panelForUnfinishedStackView.isHidden = reached
        
        imageSlider.setProject(p: projectInfo)
        imageSlider.parentController = self

        descriptionLabel.text = projectInfo.description

        self.tableView.layoutIfNeeded()
    }
    
    func silentReload() {
        let pId = self.projectInfo.id
        Server.getProjectInfo(projectId: pId, successCompletion: { [weak self] projectInfo in
            if let s = self {
                s.projectInfo = projectInfo
                s.setProjectInfo()
                s.reloaded = false
                s.tableView.layoutSubviews()
            }
            }, onErrorCompletion: nil)
    }
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        label_ReachedForFinished.text = "LABEL_REACHED".localized
//        lblGoalTitle.text = "LABEL_GOAL".localized
//        lblReachedTitle.text = "LABEL_REACHED".localized
//        lblDonateTitle.text = "LABEL_HELP".localized
//        label_Finished.text = "TEXTFINISHED".localized
//
//        enclosingViewController?.button_Finished.isHidden = true
//
//        self.tableView.backgroundView = nil
//        self.tableView.backgroundColor = UIColor(red: 238/255, green: 242/255, blue: 244/255, alpha: 1.0)
////        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
//        self.tableView.layoutIfNeeded()
//
//        descriptionLabel.text = projectInfo.description
//
//        tableView.layoutIfNeeded()
//    }
    
    func getCardType(digits: String) -> String {
        var count = 0
        var result = ""
        
        let cardDict = [
            "Visa" : "^4",
            "Master Card" : "^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)",
            "Maestro" : "^(?:5[0678]\\d\\d|6304|6390|67\\d\\d)",
            "Mir" : "^22[0-9]",
        ]
        
        for (name, pattern) in cardDict {
            do {
                let regex = try NSRegularExpression(pattern: pattern, options: .allowCommentsAndWhitespace)
                let fit = regex.matches(in: digits, options: .anchored, range: NSRange(location: 0, length: digits.count)).count > 0
                if fit {
                    count += 1
                    result = name
                }
            } catch {
                return "UnknownCard"
            }
        }
        
        if count == 1 {
            return result
        }
        return "UnknownCard"
    }
    
    static func storyboardInstance() -> ProjectInfoScrollableViewController? {
        let storyboard = UIStoryboard(name: "ProjectInfo", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ProjectInfoScrollable") as? ProjectInfoScrollableViewController
    }
    
}

//MARK: - Actions
extension ProjectInfoScrollableViewController {
    
    @IBAction private func plusButtonTapped(_ sender: UIButton) {
        self.donateValueLabel.becomeFirstResponder()
    }
    
    @IBAction private func shareButtonTapped(_ sender: UIButton) {
        let shareText = "SHARE_TEXT".localized
        let text = "\(shareText) @\(projectInfo.tag) https://tooba.site/m/\(projectInfo.tag)"
        
        let activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]
        
        present(activityViewController, animated: true, completion: nil)
    }
    
}
