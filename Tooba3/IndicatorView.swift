//
//  IndicatorView.swift
//  Tooba3
//
//  Created by Hasan on 31/07/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import UIKit

class IndicatorView : UIView {
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
}
