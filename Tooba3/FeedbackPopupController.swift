//
//  FeedbackPopupController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 23/12/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class FeedbackPopupController : UIViewController {
    @IBOutlet weak var lbl_Thanks: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_Status.text = "YOUR_FEEDBACK_HAS_BEEN_SENT".localized.uppercased()
        lbl_Thanks.text = "FEEDBACK_THANKS".localized.uppercased()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
        super.touchesEnded(touches, with: event)
    }
}
