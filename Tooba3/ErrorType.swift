//
// Created by Admin on 03/12/2016.
// Copyright (c) 2016 greenworld. All rights reserved.
//

import Foundation

enum ErrorType {
    case NetworkError
    case AuthorizationError
    case Unauthorized
    case ServerError
    case DatabaseError
    case Unknown
    case InvalidData
    case InvalidCredentials
    case Conflict
}
