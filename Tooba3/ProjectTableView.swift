//
//  ProjectTableView.swift
//  Tooba3
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit
import Kingfisher

class ProjectTableView: UITableView, UITableViewDelegate, UITableViewDataSource {
    var projects : [Project] = []
    let customRowHeight : CGFloat = UIScreen.main.bounds.width * 459.0 / 375.0
    var listType : ProjectListType = ProjectListType.active
    var myRefreshControl: UIRefreshControl? = nil
    
    var langChecked = false

    var itemsOnPage = 20
    var endPage = -1

    var backView : BackgroundView? = nil
    
    var pagesLoading : Set<Int> = []
    var pagesLoaded : Set<Int> = []

    @objc func pullRefreshPage() {
        pagesLoading.removeAll()
        pagesLoaded.removeAll()
    }
    
    func getCurrentLanguage() -> String {
        let langStr = Locale.current.languageCode ?? "ru"
        
        return langStr == "ru" ? langStr : "en"
    }
    
    func isLanguageChanged() -> Bool {
        let userDefaults = UserDefaults.standard
        if let lang = userDefaults.string(forKey: "LANG") {
            return lang != getCurrentLanguage()
        } else {
            return true
        }
    }
    
    func setCurrentLanguage() {
        let lang = getCurrentLanguage()
        let userDefaults = UserDefaults.standard
        userDefaults.set(lang, forKey: "LANG")
    }
    
    func loadPagesWithLanguageCheck(forced: Bool = false) {
        if self.langChecked {
            loadCurrentPages(forced: forced)
            return
        }
        
        if !self.isLanguageChanged() {
            self.langChecked = true
            loadCurrentPages(forced: forced)
            return
        }
        
        let lang = self.getCurrentLanguage()
        Server.SetLanguate(lang: lang, SUCCESS: { [unowned self] in
            self.setCurrentLanguage()
            self.langChecked = true
            self.loadCurrentPages(forced: forced)
        }) { [unowned self] in
            print("Can't set the language!")
        }
    }
    
    func loadCurrentPages(forced: Bool = false) {
        let pageHeight = Double(itemsOnPage) * Double(customRowHeight)
        let currentOffset = self.contentOffset.y
        
        var p1 = 0
        var p2 = 1
        
        p1 = Int((Double(currentOffset) + pageHeight / 2) / pageHeight)
        p2 = p1 + 1
        
        if forced {
            pagesLoaded.remove(p1)
            pagesLoaded.remove(p2)
        }
        
        getPage(pageNumber: p1)
        getPage(pageNumber: p2)
    }
    
    func getPage(pageNumber : Int) {
        if pagesLoaded.contains(pageNumber) {
            return
        }
        
        if pageNumber < 1 {
            return
        }
        
        if endPage != -1 && pageNumber > endPage {
            return
        }
        
        if !pagesLoading.contains(pageNumber) {
            pagesLoading.insert(pageNumber)
            if self.myRefreshControl?.isRefreshing == false && self.projects.count == 0 {
                self.showReloadView()
            }
            Server.GetProjectList(listType: listType, itemsOnPage : itemsOnPage, pageNumber : pageNumber,
                    YES: { [unowned self] projects in
                        self.hideReloadView()
                        self.myRefreshControl?.endRefreshing()
                        self.backView?.label.text = ""

                        if self.endPage == -1 && projects.count < self.itemsOnPage {
                            self.endPage = pageNumber
                        }

                        let itemsCount = (pageNumber - 1) * self.itemsOnPage + projects.count

                        DispatchQueue.main.async { [unowned self] in
                            if pageNumber == self.endPage {
                                if projects.count == self.itemsOnPage {
                                    self.endPage = -1
                                } else if projects.count == 0 {
                                    self.endPage = -1
                                    while self.projects.count > itemsCount {
                                        self.projects.removeLast()
                                    }
                                } else {
                                    while self.projects.count > itemsCount {
                                        self.projects.removeLast()
                                    }
                                }
                            }

                            while self.projects.count < itemsCount {
                                self.projects.append(Project())
                            }

                            let startIndex = (pageNumber - 1) * self.itemsOnPage

                            if projects.count > 0 {
                                for i in 0 ... projects.count - 1 {
                                    self.projects[startIndex + i] = projects[i]
                                }
                            }

                            self.pagesLoaded.insert(pageNumber)

                            self.reloadData()
                        }

                        self.pagesLoading.remove(pageNumber)
                }, NO: { [unowned self] et, msg in
                    self.hideReloadView()
                    self.backView?.label.text = "CONNECTION_ERROR".localized
                    self.myRefreshControl?.endRefreshing()
                    self.pagesLoading.remove(pageNumber)
            })
        }
    }
    
    var RowSelected : ((Project, Int) -> ())? = nil
    var PayPressed : ((Project, Int, Double) -> ())? = nil
    var SharePressed : ((Project, Int) -> ())? = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        let nib = UINib(nibName: "ProjectTableViewCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "ProjectCell")
        self.dataSource = self
        self.delegate = self
        self.allowsSelection = true
        self.separatorStyle = .none
        self.sectionHeaderHeight = 20
        self.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: 20))
        
        backView = BackgroundView(frame: self.bounds)
        self.backgroundView = backView
        backView?.label.text = ""
        
        self.myRefreshControl = UIRefreshControl();
        self.addSubview(self.myRefreshControl!);
        self.myRefreshControl?.addTarget(self, action: #selector(pullRefreshPage), for: .valueChanged)
    }
    
    func setProjects(ps : [Project]) {
        projects = ps
        self.reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (!self.isDecelerating && self.isDragging) {
            for c in self.visibleCells {
                let cell = c as! ProjectTableViewCell
                cell.txtPay.resignFirstResponder()
            }
        }
        loadPagesWithLanguageCheck()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func headerView(forSection section: Int) -> UITableViewHeaderFooterView? {
        return UITableViewHeaderFooterView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: 20))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dequeueReusableCell(withIdentifier: "ProjectCell") as! ProjectTableViewCell
        
        let p = projects[indexPath.row]
        
        cell.aboutProjectPressedEvent = { [weak self] in
            if let A = self {
                A.RowSelected?(p, indexPath.row)
            }
        }
        
        cell.btnPayPressedEvent = {[weak self] amount in
            self?.PayPressed?(p, indexPath.row, amount)
        }
        
        cell.shareButtonPressedEvent = {[weak self] in
            self?.SharePressed?(p, indexPath.row)
        }
        
        cell.setProject(p: p, row: indexPath.row)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let c = cell as! ProjectTableViewCell
        c.img.kf.cancelDownloadTask()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return customRowHeight
    }
    
}
