//
//  ProjectListViewController.swift
//  Tooba3
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit
import SideMenu

class ProjectListViewController : UIViewController, UIGestureRecognizerDelegate {
    var searchButton: UIBarButtonItem!
    var historyButton: UIBarButtonItem!
    var backView : BackgroundView? = nil
    var selectedProjectList : ProjectListType = .active
    static public var silentPaymentLimit = 499.0
    
    @IBOutlet weak var tableView: ProjectTableView!
    
    var projectIdToFetch = -1
    var projectTagToFetch: String?
    var loaded = false
    
    static public func ColorForTitleView(row: Int) -> UIColor {
        if row % 3 == 0 {
            return UIColor(red: 185/255.0, green: 104/255.0, blue: 199/255.0, alpha: 1.0)
        } else if row % 3 == 1 {
            return UIColor(red: 120/255.0, green: 192/255.0, blue: 66/255.0, alpha: 1.0)
        } else {
            return UIColor(red: 244/255.0, green: 193/255.0, blue: 55/255.0, alpha: 1.0)
        }
    }
    
    @objc func keyboardDidShow(_ notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            let keyboardTop = keyboardRectangle.origin.y
            
            var activeCellNullable: ProjectTableViewCell? = nil
            
            for cell in tableView.visibleCells {
                if (cell as! ProjectTableViewCell).txtPay.isFirstResponder {
                    activeCellNullable = (cell as! ProjectTableViewCell)
                    break
                }
            }
            
            if let activeCell = activeCellNullable {
                let txtPay = activeCell.txtPay
                let p = (txtPay?.superview?.convert(txtPay!.frame.origin, to: self.tableView).y)! - self.tableView.contentOffset.y
                let diff = keyboardRectangle.origin.y - p - 64 - txtPay!.frame.height - 15
                
                if diff < 0 {
                    var cf = self.tableView.contentOffset
                    cf.y = cf.y - diff
                    UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                        self.tableView.contentOffset = cf
                        }, completion: nil)
                }
                
                let rect = activeCell.txtPay.frame.origin.y + activeCell.bounds.origin.y
            }
        }
    }
    
    @objc func keyboardDidHide(_ notification: NSNotification) {
        if tableView.contentSize.height < tableView.frame.height {
            return
        }
        let diff = tableView.contentSize.height - tableView.frame.height - tableView.contentOffset.y
        if diff < 0 {
            var cf = self.tableView.contentOffset
            cf.y = cf.y + diff
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: { [unowned self] in
                self.tableView.contentOffset = cf
            }, completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let intro = UserDefaults.standard.string(forKey: "INTRO") ?? "NO"
        if (intro != "YES") {
            UserDefaults.standard.set("YES", forKey: "INTRO")
            let slides = IntroductionSlider()
            self.present(slides, animated: true, completion: nil)
        }

        SideMenuManager.default.menuFadeStatusBar = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
        
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor(red: 239/255, green: 243/255, blue: 245/255, alpha: 1.0)
        
        self.backView = BackgroundView(frame: self.view.bounds);
        self.view.addSubview(self.backView!);

        self.backView?.label.text = "PRESS_TO_RELOAD".localized
        self.backView?.isHidden = true

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.reloadTapped))
        tap.delegate = self
        self.backView?.addGestureRecognizer(tap)

        if !Db.isAuthorized() {
            self.tableView.isHidden = true
            self.loginWithAnonymousUser()
        } else {
            self.loaded = true
        }

        self.searchButton = UIBarButtonItem(image: #imageLiteral(resourceName: "SearchButton"), style: .plain, target: self, action: #selector(self.searchPressed))
        self.historyButton = UIBarButtonItem(image: #imageLiteral(resourceName: "MenuButtonLines"), style: .plain, target: self, action: #selector(self.historyPressed))
        self.navigationItem.rightBarButtonItem = self.searchButton
        self.navigationItem.leftBarButtonItem = self.historyButton
        let titleView = UIImageView(image: #imageLiteral(resourceName: "ToobaLogo"))
        titleView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = titleView

        self.navigationItem.setHidesBackButton(true, animated: false)

        tableView.RowSelected = { [unowned self] (p, i) in
            let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
            vc.projectInfo = p
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        tableView.SharePressed = { [unowned self] (project, index) in
            // text to share
            let beginning_text = "SHARE_TEXT".localized
            let text = "\(beginning_text) @\(project.tag) https://tooba.site/m/\(project.tag)"
            
            // set up activity view controller
            let objectsToShare: [AnyObject] = [ text as AnyObject ]
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
        
        tableView.PayPressed = { [unowned self] (project, index, amount) in
            if amount >= ProjectListViewController.silentPaymentLimit {
                Alerts.ShowActionSheet(sender: self, title: "DO_YOU_REALLY_WANT_TO_PAY".localized + " \(project.currency.toString(amount))?", items: ["YES".localized, "NO".localized], callback: { [weak self] choice in
                    if choice == 0 {
                        self?.showSelector(projectInfo: project, amount: amount)
                    }
                }, completion: nil)
            } else {
                self.showSelector(projectInfo: project, amount: amount)
            }
        }
    }
    
    func showSelector(projectInfo: Project, amount: Double) {
        self.view.showReloadView()
        Server.yandexKassaLinkedBankCards(successCompletion: { [unowned self] bankCardItems in
            self.view.hideReloadView()
            var bankCardMasks = bankCardItems.map({$0.maskWithAsterisks()})
            
            if bankCardItems.count == 0 {
                self.payAction(projectInfo: projectInfo, amount: amount)
                return
            }
            
            self.payAction(projectInfo: projectInfo, amount: amount, bankCardItems[0].cardMask)
            return
            }, onErrorCompletion: { [unowned self] in
                self.view.hideReloadView()
                Alerts.showErrorAlertWithOK(sender: self, title: "ERROR".localized, message: "CANT_MAKE_A_PAYMENT".localized, completion: nil)
        })
    }
    
    func payAction(projectInfo: Project, amount: Double, _ cardMask: String = "") {
        self.view.showReloadView()
        
        Server.yandexKassaPayment(projectId: projectInfo.id, amount: amount, cardMask: cardMask, successCompletion: { [unowned self] urlString in
            self.view.hideReloadView()
            
            if urlString == "Ok" {
                let vc = MotivationViewController.storyboardInstance()
                vc.projectInfo = projectInfo
                self.present(vc, animated: true, completion: { [unowned self] in
//                    self.silentReload()
                })
            } else {
                let url = URL(string: urlString)!
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.openURL(url);
                }
            }
            }, onErrorCompletion: { [unowned self] in
                self.view.hideReloadView()
                Alerts.showErrorAlertWithOK(sender: self, title: "Ошибка", message: "CANT_MAKE_A_PAYMENT".localized, completion: nil)
        })
    }

    @objc func reloadTapped(sender: UITapGestureRecognizer? = nil) {
        self.loginWithAnonymousUser()
    }
    
    func openProjectWithId(id: Int) {
        let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
        vc.projectIdToFetch = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openProjectWithTag(tag: String) {
        let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
        vc.projectTagToFetch = tag
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func loginWithAnonymousUser() {
        self.view.showReloadView()
        self.backView?.isHidden = true
        Server.SignUpWithoutRegistration(SUCCESS: { [unowned self] ud in
            let username = ud.username
            let password = ud.password

            Server.SignInAsTempUser(username: username, password: password, YES: { [unowned self] cred in
                self.navigationController?.isNavigationBarHidden = false
                self.tableView.isHidden = false
                self.view.hideReloadView()
                self.tableView.loadPagesWithLanguageCheck()
                
                self.loaded = true
                
                if (self.projectIdToFetch > -1) {
                    let id = self.projectIdToFetch
                    self.projectIdToFetch = -1
                    self.openProjectWithId(id: id)
                } else if let pTag = self.projectTagToFetch {
                    self.projectTagToFetch = nil
                    self.openProjectWithTag(tag: pTag)
                }
                
                
//                print("Fetching project after signing in: \(self.projectIdToFetch)")
//                Alerts.ShowErrorAlertWithOK(sender: self, title: "Fetching after signing in", message: "\(self.projectIdToFetch)", completion: nil)
            }, NO: { [unowned self] et, str in
                self.view.hideReloadView()
                self.backView?.isHidden = false
                self.navigationController?.isNavigationBarHidden = true
                Alerts.showErrorAlertWithOK(sender: self, title: "ERROR", message: "CANT_ENTER_PRESS_OK_TO_REPEAT".localized, completion: nil)
            })

        }, ERROR: { [unowned self] in
            self.view.hideReloadView()
            self.navigationController?.isNavigationBarHidden = true

            self.backView?.isHidden = false
            Alerts.showErrorAlertWithOK(sender: self, title: "ERROR", message: "CANT_ENTER_PRESS_OK_TO_REPEAT".localized, completion: nil)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !Db.isAuthorized() { return }

        if (self.projectIdToFetch > -1) {
            let id = self.projectIdToFetch
            self.projectIdToFetch = -1
            self.openProjectWithId(id: id)
        } else if let pTag = self.projectTagToFetch {
            self.projectTagToFetch = nil
            self.openProjectWithTag(tag: pTag)
        }
//        print("Fetching project on appear: \(self.projectIdToFetch)")
//        Alerts.ShowErrorAlertWithOK(sender: self, title: "Fetching on appear", message: "\(self.projectIdToFetch)", completion: nil)
        
        self.tableView.loadPagesWithLanguageCheck(forced: true)
    }

    @objc func searchPressed() {
        self.performSegue(withIdentifier: "SearchSegue", sender: self)
    }
    
    @objc func historyPressed() {
         self.performSegue(withIdentifier: "openSideMenu", sender: self)
    }
}
