//
//  PaymentHistoryCell.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 15/01/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class PaymentHistoryProjectCell: UITableViewCell {
    @IBOutlet weak var lblTag: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var labelForDonated: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDonated: UILabel!
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var coloredLine: UIView!
    @IBOutlet weak var finishedDateSection: UIView!
    @IBOutlet weak var checkmarkView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cellContentView.roundCorners(corners: .allCorners, radius: 8)
        img.roundCorners(corners: .allCorners, radius: img.frame.height / 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLikeCount(likeCount : Int) {
        lblLikeCount.text = "\(likeCount)"
    }
}
