//
//  ProjectTableView.swift
//  Tooba3
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit
import Kingfisher

class ProjectTableViewWithoutPagination: UITableView, UITableViewDelegate, UITableViewDataSource {
    var projects : [Project] = []
    let customRowHeight : CGFloat = UIScreen.main.bounds.width * 459.0 / 375.0
    var listType : ProjectListType = ProjectListType.active
    
    var RowSelected : ((Project, Int) -> ())? = nil
    var PayPressed : ((Project, Int, Double) -> ())? = nil
    var SharePressed : ((Project, Int) -> ())? = nil
    
    var backView : BackgroundView? = nil
    
    var Scrolled: (() -> ())? = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        let nib = UINib(nibName: "ProjectTableViewCell", bundle: nil)
        self.register(nib, forCellReuseIdentifier: "ProjectCell")
        self.dataSource = self
        self.delegate = self
        self.allowsSelection = true
        self.separatorStyle = .none
        
        self.sectionHeaderHeight = 20
        self.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: 20))
        
//        backView = BackgroundView(frame: self.bounds)
//        self.backgroundView = backView
//        backView?.label.text = ""
    }
    
    func setProjects(ps : [Project]) {
        self.projects = ps
        self.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//        if let rowSelected = RowSelected {
//            rowSelected(projects[indexPath.row], indexPath.row)
//        }
//        return nil
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dequeueReusableCell(withIdentifier: "ProjectCell") as! ProjectTableViewCell
        let p = projects[indexPath.row]
        
        cell.aboutProjectPressedEvent = { [weak self] in
            if let A = self {
                A.RowSelected?(p, indexPath.row)
            }
        }
        
        cell.btnPayPressedEvent = {[weak self] amount in
            self?.PayPressed?(p, indexPath.row, amount)
        }
        
        cell.shareButtonPressedEvent = {[weak self] in
            self?.SharePressed?(p, indexPath.row)
        }
        
        cell.setProject(p: p, row: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let c = cell as! ProjectTableViewCell
        c.img.kf.cancelDownloadTask()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return customRowHeight
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (!self.isDecelerating && self.isDragging) {
            for c in self.visibleCells {
                let cell = c as! ProjectTableViewCell
                cell.txtPay.resignFirstResponder()
            }
            self.Scrolled?()
        }
    }
}

