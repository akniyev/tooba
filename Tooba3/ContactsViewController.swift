//
//  ContactsViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 12/11/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class ContactsViewController : UIViewController {
    @IBOutlet weak var btnLicense: UIButton!
    @IBOutlet weak var txtAbout: UITextView!
    @IBAction func btnSiteTouched(_ sender: Any) {
        UIApplication.shared.open(URL(string: "https://tooba.site")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func btnSupportTouched(_ sender: Any) {
        UIApplication.shared.open(URL(string: "mailto:info@tooba.site")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func btnPhoneTouched(_ sender: Any) {
        UIApplication.shared.open(URL(string: "tel:+79031231234")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func btnTofTouched(_ sender: Any) {
        UIApplication.shared.open(URL(string: "https://tooba.site/agreement/user")!, options: [:], completionHandler: nil)
    }
    
    func getAttributedAboutText() -> NSAttributedString {
        let title = "ABOUT_TITLE".localized
        let text = "ABOUT_TEXT".localized
        
        let quote = title + "\n" + text
        
        
        let titleFont = UIFont.boldSystemFont(ofSize: 17)
        
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .center
        
        let titleAttributes: [NSAttributedString.Key: Any] = [
            .font: titleFont,
            .paragraphStyle: titleParagraphStyle,
            ]
        
        let textFont = UIFont.systemFont(ofSize: 14)
        let textParagraphStyle = NSMutableParagraphStyle()
        textParagraphStyle.alignment = .left
        
        let textAttributes: [NSAttributedString.Key: Any] = [
            .font: textFont,
            .paragraphStyle: textParagraphStyle,
            ]
        
        let attributedAboutText = NSMutableAttributedString(string: quote)
        attributedAboutText.addAttributes(titleAttributes, range: NSRange(location: 0, length: title.count))
        attributedAboutText.addAttributes(textAttributes, range: NSRange(location: title.count + 1, length: text.count))
        
        return attributedAboutText
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "TITLE_OUR_CONTACTS".localized
        self.btnLicense.setTitle("LICENSE_AGREEMENT".localized, for: .normal)
        
        self.txtAbout.attributedText = getAttributedAboutText()
    }
}
