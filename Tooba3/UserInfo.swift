//
//  UserInfo.swift
//  Tooba3
//
//  Created by Admin on 29/11/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import Foundation

class UserInfo {
    var name : String? = nil
    var gender : Gender? = nil
    var birthday : Date? = nil
    var isEmailConfirmed : Bool = false
    
    func setBirthdate (day : Int, month : Int, year: Int) {
        let calendar = NSCalendar.current
        var components = calendar.dateComponents([Calendar.Component.year, Calendar.Component.month, Calendar.Component.day], from: Date.init())
        components.day = day
        components.month = month
        components.year = year
        
        let date = calendar.date(from: components)
        birthday = date
    }
}

enum Gender {
    case Male
    case Female
    
    func toString() -> String {
        if self == Gender.Male {
            return "male"
        } else {
            return "female"
        }
    }
    
    func ToString() -> String {
        if self == Gender.Male {
            return "Male"
        } else {
            return "Female"
        }
    }
    
    static func fromString (s : String?) -> Gender? {
        if s == nil {
            return nil
        } else if s!.lowercased() == "male" {
            return .Male
        } else if s!.lowercased() == "female" {
            return .Female
        } else {
            return nil
        }
    }
}
