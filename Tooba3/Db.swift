//
//  Db.swift
//  Tooba3
//
//  Created by Admin on 24/09/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import Foundation
import GRDB

class Db {
    static func GetDbDirectoryPath() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        return paths[0]
    }
    
    static func GetDbFilePath() -> String {
        return GetDbDirectoryPath() + "/credentials.sqlite"
    }

    static func GetTempDbFilePath() -> String {
        return GetDbDirectoryPath() + "/tempuser.sqlite"
    }

    static func isTempDbFileExists() -> Bool {
        let path = GetTempDbFilePath()

        let fileManager = FileManager.default

        return fileManager.fileExists(atPath: path)
    }

    static func isDbFileExists() -> Bool {
        let path  = GetDbFilePath()
        
        let fileManager = FileManager.default
        
        return fileManager.fileExists(atPath: path)
    }

    static func CreateTempUserDbFile() -> Bool {
        if isTempDbFileExists() {
            return true
        }

        do {
            let dbQueue = try DatabaseQueue(path: GetTempDbFilePath())
            try dbQueue.inDatabase { db in
                try db.execute(
                        "CREATE TABLE tempuser (" +
                                "id INTEGER PRIMARY KEY, " +
                                "username TEXT NOT NULL, " +
                                "password TEXT NOT NULL, " +
                                "active INTEGER NOT NULL " +
                                ")"
                )
            }
        }
        catch {
            return false
        }

        return true
    }
    
    static func CreateDbFile() -> Bool {
        if isDbFileExists() {
            return true
        }
        
        do {
            let dbQueue = try DatabaseQueue(path: GetDbFilePath())
            try dbQueue.inDatabase { db in
                try db.execute(
                    "CREATE TABLE credentials (" +
                        "id INTEGER PRIMARY KEY, " +
                        "email TEXT NOT NULL, " +
                        //"password TEXT NOT NULL, " +
                        "accessToken TEXT NOT NULL, " +
                        "accessTokenCreated DOUBLE NOT NULL, " +
                        "refreshToken TEXT NOT NULL," +
                        "refreshTokenCreated DOUBLE NOT NULL," +
                        "socialToken TEXT NOT NULL, " +
                        "loginType TEXT NOT NULL" +
                    ")"
                )
            }
        }
        catch {
            return false
        }
        
        return true
    }

    static func writeTempUserData(t : TempUserData?) -> Bool {
        if !CreateTempUserDbFile() {
            return false
        }

        if !isTempDbFileExists() {
            return false
        }

        do {
            let dbQueue = try DatabaseQueue(path: GetTempDbFilePath())

            try dbQueue.inDatabase { db in
                try db.execute("DELETE FROM tempuser")

                if let ud = t {
                    try db.execute(
                            "INSERT INTO tempuser (username, password, active) " +
                                    "VALUES(?, ?, ?)",
                            arguments:  [ud.username, ud.password, ud.active ? 1 : 0]
                    )
                }

            }
        }
        catch {
            return false
        }

        return true
    }
    
    static func writeCredentials(c : Credentials?) -> Bool {
        if !CreateDbFile() {
            return false
        }
        
        if !isDbFileExists() {
            return false
        }
        
        
        do {
            let dbQueue = try DatabaseQueue(path: GetDbFilePath())
            
            try dbQueue.inDatabase { db in                
                try db.execute(
                    "DELETE FROM credentials"
                )

                if let cred = c {
                    if cred.loginType == .Email {
                        print("ATTENTION!")
                    }
                    print("email: " + cred.email)
                    try db.execute(
                        "INSERT INTO credentials (email, accessToken, accessTokenCreated, refreshToken, refreshTokenCreated, socialToken, loginType) " +
                        "VALUES(?, ?, ?, ?, ?, ?, ?)",
                        arguments:  [cred.email,
                                     cred.access_token,
                                     cred.get_access_token_created_unixtime(),
                                     cred.refresh_token,
                                     cred.get_refresh_token_created_unixtime(),
                                     cred.social_token,
                                     cred.loginType.toString()]
                    )
                }
            }
        }
        catch {
            return false
        }
        
        return true
    }

    static func readTempUserData() -> TempUserData? {
        var result : TempUserData? = nil

        if !isTempDbFileExists() {
            return result
        }

        do {
            let dbQueue = try DatabaseQueue(path: GetTempDbFilePath())

            dbQueue.inDatabase { db in
                let rows = try! Row.fetchAll(db, "SELECT * FROM tempuser")
                if rows.count > 0 {
                    let row = rows[0]
                    let ud = TempUserData()
                    ud.username = row["username"]
                    ud.password = row["password"]
                    ud.active = row["active"] == 1

                    result = ud
                }
            }
        }
        catch {

        }

        return result
    }
    
    static func readCredentials() -> Credentials? {
        var result : Credentials? = nil
        
        if !CreateDbFile() {
            return result
        }
        
        do {
            let dbQueue = try DatabaseQueue(path: GetDbFilePath())
            
            dbQueue.inDatabase { db in
                let rows = try! Row.fetchAll(db, "SELECT * FROM credentials")
                if rows.count > 0 {
                    let row = rows[0]
                    let cred = Credentials()
                    cred.access_token = row["accessToken"]
                    cred.set_access_token_created_from_unixtime(unixtime: row["accessTokenCreated"])
                    cred.refresh_token = row["refreshToken"]
                    cred.set_refresh_token_created_from_unixtime(unixtime: row["refreshTokenCreated"])
                    cred.email = row["email"]
                    cred.social_token = row["socialToken"]
                    cred.loginType = LoginType.fromString(s: row["loginType"])
                    result = cred
                }
            }
        }
        catch {

        }
        return result
    }
    
    static func isAuthorized() -> Bool {
        return readCredentials() != nil
    }

    static func isTempUser() -> Bool {
        if let ud = readTempUserData() {
            return ud.active
        } else {
            return false
        }
    }

    static func deactivateTempUser() {
        var ud = readTempUserData()
        if ud?.active == true {
            ud?.active = false
            writeTempUserData(t: ud)
        }
    }

    static func activateTempUser() {
        var ud = readTempUserData()
        if ud != nil && ud?.active == false {
            ud?.active = true
        }
        writeTempUserData(t: ud)
    }
}
