//
// Created by Admin on 29/07/16.
// Copyright (c) 2016 greenworld. All rights reserved.
//

import UIKit

struct CachedImage {
    let timestamp : Double
    let image : UIImage

    init(_image : UIImage) {
        image = _image
        timestamp = Date().timeIntervalSince1970
    }
}

class ImageDownloaderOld {
    static fileprivate var cache = NSCache<NSString, UIImage>()

    static fileprivate var imagePool = [UIImageView: String?]()

    static fileprivate var imagePool2 = NSMutableDictionary()

    static func loadImageFromUrl(_ urlString: String, imageView: UIImageView) {
        imagePool[imageView] = urlString

        if (!downloadStarted) {
            downloadStarted = true
            beginImageDownload()
        }
    }

    static fileprivate var downloadStarted = false

    static func beginImageDownload() {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            while (true) {
                let el : (UIImageView, String?)? = imagePool.popFirst()

                if (el == nil || el!.1 == nil) {
                    continue
                }

                let urlString = el!.1!

                if let cachedImage = (cache.object(forKey: urlString as NSString) as UIImage?) {
                    DispatchQueue.main.async(execute: {
                        el!.0.image = cachedImage
                    })
                } else {
                    let url = URL(string: urlString)

                    if (url == nil) {
                        continue
                    }

                    let data = try? Data(contentsOf: url!)

                    if (data == nil) {
                        continue
                    }

                    let img = UIImage(data: data!)

                    if (img != nil) {
                        cache.setObject(img!, forKey: urlString as NSString)
                    }

                    DispatchQueue.main.async(execute: {
                        el!.0.image = img
                    })
                }
            }
        }
    }
}


