//
//  BankCardsManagerController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 04/10/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class BankCardsManagerController : UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var bankCards: [BankCardItem] = []
    
    var backView : BackgroundView? = nil
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bankCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bank_card_cell") as! BankCardCell
        
        cell.label.text = bankCards[indexPath.row].cardMask
//        cell.subtitle_label.text = "SWIPE_LEFT_TO_DELETE".localized
        let row = indexPath.row
        cell.onDeleteTouched = { [unowned self] () in
            Alerts.ShowAlert(sender: self, title: "ATTENTION".localized, message: "DO_U_WANT_TO_DELETE_THIS_CARD".localized, preferredStyle: .alert, actions: [
                UIAlertAction(title: "YES".localized, style: .default) { [unowned self] action in
                    self.view.showReloadView()
                    Server.YandexKassaDeleteLinkedBankCard(cardMask: self.bankCards[row].cardMask, SUCCESS: {
                        self.view.hideReloadView()
                        self.reloadDataFromServer()
                    }, ERROR: {
                        self.view.hideReloadView()
                        Alerts.showErrorAlertWithOK(sender: self, title: "ERROR".localized, message: "CANT_DELETE_BANK_CARD".localized, completion: nil)
                    })
                },
                UIAlertAction(title: "NO".localized, style: .default) { action in
                    tableView.setEditing(false, animated: true)
                }
                ], completion: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 202
    }
    
    private func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Alerts.ShowAlert(sender: self, title: "ATTENTION".localized, message: "DO_U_WANT_TO_DELETE_THIS_CARD".localized, preferredStyle: .alert, actions: [
                UIAlertAction(title: "YES".localized, style: .default) { [unowned self] action in
                    self.view.showReloadView()
                    Server.YandexKassaDeleteLinkedBankCard(cardMask: self.bankCards[indexPath.row].cardMask, SUCCESS: {
                        self.view.hideReloadView()
                        self.reloadDataFromServer()
                    }, ERROR: {
                        self.view.hideReloadView()
                        Alerts.showErrorAlertWithOK(sender: self, title: "ERROR".localized, message: "CANT_DELETE_BANK_CARD".localized, completion: nil)
                    })
                },
                UIAlertAction(title: "NO".localized, style: .default) { [unowned self] action in
                    tableView.setEditing(false, animated: true)
                }
                ], completion: nil)
        }
    }
    
    func reloadDataFromServer() {
        self.view.ShowWhiteReloadView()
        Server.yandexKassaLinkedBankCards(successCompletion: { [unowned self] bankCards in
            self.bankCards = bankCards
            self.tableView.reloadData()
            self.view.hideReloadView()
            
            if self.bankCards.count == 0 {
                self.backView?.isHidden = false
            }
            
            }, onErrorCompletion: {
                self.view.hideReloadView()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "LINKED_BANK_CARDS".localized
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.backView = BackgroundView(frame: self.view.bounds);
        self.view.addSubview(self.backView!);
        
        self.backView?.label.text = "NO_SAVED_CARDS".localized
        self.backView?.isHidden = true
        
        self.reloadDataFromServer()
    }
}
