//
//  FormTableView.swift
//  Tooba3
//
//  Created by Admin on 19/09/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class FormTableView: UITableView, UITableViewDataSource, UITableViewDelegate {
    private var data : [[CellInfo]] = []
    private var _data : [[CellInfo]] = []
    
    func setData (d : [[CellInfo]]) {
        data = d
        _data = d
        reloadData()
        print("setData")
    }
    
    func getData() -> [[CellInfo]] {
        var ndata = data
        for sectionIndex in (0..<data.count) {
            for row in (0..<data[sectionIndex].count) {
                let cell = self.cellForRow(at: IndexPath(row: row, section: sectionIndex))
                
                if cell != nil {
                    let cell1 = cell as! CellInfoReturner
                    let c = cell1.getCellData()
                    ndata[sectionIndex][row] = c
                }
            }
        }
        return ndata
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.register(UINib(nibName: "FormTableViewCell", bundle: nil), forCellReuseIdentifier: "String")
        
        self.register(UINib(nibName: "ButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "Button")
        
        self.register(UINib(nibName: "PasswordTableViewCell", bundle: nil), forCellReuseIdentifier: "Password")
        
        self.register(UINib(nibName: "EmailTableViewCell", bundle: nil), forCellReuseIdentifier: "Email")
        
        self.register(UINib(nibName: "DateTableViewCell", bundle: nil), forCellReuseIdentifier: "Date")
        
        self.delegate = self
        self.dataSource = self
    }
    
    //TableView Data source and delegate
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = data[indexPath.section][indexPath.row]
        
        if c.type == .Button {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Button") as! ButtonTableViewCell
            cell.button.setTitle(c.label, for: .normal)
            cell.callback = c.callback
            return cell
        } else if c.type == .String {
            let cell = tableView.dequeueReusableCell(withIdentifier: "String") as! FormTableViewCell
            cell.lblFieldName.text = c.label
            cell.txtValue.text = c.value
            cell.txtValue.placeholder = c.placeholder
            return cell
        } else if c.type == .Email {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Email") as! EmailTableViewCell
            cell.lblFieldName.text = c.label
            cell.txtValue.text = c.value
            cell.txtValue.placeholder = c.placeholder
            return cell

        } else if c.type == .Password {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Password") as! PasswordTableViewCell
            cell.lblFieldName.text = c.label
            cell.txtValue.text = c.value
            cell.txtValue.placeholder = c.placeholder
            
            return cell

        } else if c.type == .DateTime {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Date") as! DateTableViewCell
            cell.setDate(nd: c.date)
            cell.lblLabel.text = c.label
            
            return cell
        } else {
            return UITableViewCell()
        }

    }
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
}

class CellInfo {
    var type = CellTypes.String
    var value = "Value"
    var label = "Label"
    var placeholder = "Placeholder"
    var callback : ((_ sender : UIButton) -> ())? = nil
    var date : Date? = nil
//    var color : UIColor = UIColor.clear
    
    init() {
        
    }
    
    init(type : CellTypes, label : String, value : String, placeholder : String, callback : ((_ sender : UIButton) -> ())?) {
        self.type = type
        self.value = value
        self.label = label
        self.placeholder = placeholder
        self.callback = callback
    }
    
    init(type : CellTypes, label : String, value : String, placeholder : String) {
        self.type = type
        self.value = value
        self.label = label
        self.placeholder = placeholder
        self.callback = nil
    }
    
    init(type : CellTypes, label : String) {
        self.type = type
        self.label = label
        self.placeholder = ""
        self.value = ""
        self.callback = nil
    }
    
    init(type : CellTypes, label : String, date : Date?) {
        self.type = type
        self.label = label
        self.placeholder = ""
        self.value = ""
        self.callback = nil
        self.date = date
    }
}

enum CellTypes {
    case String
    case Password
    case Email
    case DateTime
    case Image
    case Number
    case Button
}

protocol CellInfoReturner {
    func getCellData() -> CellInfo
}


