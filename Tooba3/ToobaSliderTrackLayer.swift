//
//  ToobaSliderTrackLayer.swift
//  CustomSliderExample
//
//  Created by Admin on 24/08/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit
import QuartzCore

class ToobaSliderTrackLayer: CALayer {
    
    weak var toobaSlider : ToobaSlider?
    
    var emptyTrackWidthRatio : CGFloat = 0.2
    var filledTrackWidthRatio : CGFloat = 0.5
    
    override func draw(in ctx: CGContext) {
        if let slider = toobaSlider {
            let cornerRadius1 = bounds.height / 6.0
            let trackBounds1 = CGRect(x: 0.0, y: bounds.height / 2 * (1 - emptyTrackWidthRatio), width: bounds.width, height: bounds.height * emptyTrackWidthRatio)
            let path1 = UIBezierPath(roundedRect: trackBounds1, cornerRadius: cornerRadius1)
            ctx.addPath(path1.cgPath)
            
            ctx.setFillColor(slider.trackColor.cgColor)
            
            ctx.addPath(path1.cgPath)
            ctx.fillPath()
            
            
            let cornerRadius2 = bounds.height / 2.0
            let trackBounds2 = CGRect(x: 0.0, y: bounds.height / 2 * (1 - filledTrackWidthRatio),
                                      width: bounds.height / 2 + (bounds.width - bounds.height) * slider.fillRatio(),
                                      height: bounds.height * filledTrackWidthRatio)
            let path2 = UIBezierPath(roundedRect: trackBounds2, cornerRadius: cornerRadius2)
            ctx.addPath(path2.cgPath)
            
            ctx.setFillColor(slider.trackFillColor1.cgColor)
            ctx.addPath(path2.cgPath)
            ctx.clip()
            // 1
            let currentContext = ctx
            
            // 2
            currentContext.saveGState();
            
            // 3
            let colorSpace = CGColorSpaceCreateDeviceRGB()

            var colorComponents : [CGFloat] = [
                1.0, 0.03, 0.0, 1.0,
                0.996, 0.714, 0.0, 1.0,
                0.678, 0.863, 0.0, 1.0,
                0.424, 0.902, 0.0, 1.0,
                0.325, 0.82, 0.0, 1.0
            ]
            
            // 6
            var locations:[CGFloat] = [0.0, 0.33, 0.75, 0.9, 1.0]
            
            // 7
            let gradient = CGGradient(colorSpace: colorSpace,colorComponents: &colorComponents,locations: &locations,count: 5)
            
            let startPoint = CGPoint(x: 0, y: self.bounds.height)
            let endPoint = CGPoint(x: self.bounds.width, y: self.bounds.height)
            
            // 8
            currentContext.drawLinearGradient(gradient!,start: startPoint,end: endPoint, options: CGGradientDrawingOptions(rawValue: 0))
            
            
            
            // 9
            currentContext.restoreGState();
        }
    }
    
}
