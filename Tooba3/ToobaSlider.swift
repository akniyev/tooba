//
//  ToobaSlider.swift
//  CustomSliderExample
//
//  Created by Admin on 24/08/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit
import QuartzCore

//@IBDesignable
class ToobaSlider: UIControl {
    func fillRatio() -> CGFloat {
        return CGFloat((self.value - self.minValue) / (self.maxValue - self.minValue))
    }
    
    func setValue (x : Double) {
        value = x
        updateLayerFrames()
    }
    
    let popView = UIView()
    let popupText = UILabel()
    
    @IBInspectable
    var minValue : Double = 0
    @IBInspectable
    var maxValue : Double = 1.0
    
    @IBInspectable
    var value : Double = 0.5 {
        didSet {
            if value < minValue {
                value = minValue
            } else if value > maxValue {
                value = maxValue
            }
            
            if value < minBound {
                value = minBound
            }
        }
    }
    
    @IBInspectable
    var minBound : Double = 0.0 {
        didSet {
            if minBound < minValue {
                minBound = minValue
            } else if value > maxValue {
                minBound = maxValue
            }
            
            if minBound > value {
                value = minBound
            }
        }
    }
    
    var trackColor : UIColor = UIColor.black
    var trackFillColor1 : UIColor = UIColor.black
    var trackFillColor2 : UIColor = UIColor.black
    var trackFillColor2pressed : UIColor = UIColor.black
    
    @IBInspectable
    var showPopup : Bool = false
    
    var popupWidth : CGFloat = 40.0
    var popupHeight : CGFloat = 20.0
    var popupDistance : CGFloat = 5.0
    
    var previousLocation : CGPoint = CGPoint()
    var previousValue : Double = 0
    
    let trackLayer = ToobaSliderTrackLayer()
    let thumbLayer = ToobaSliderThumbLayer()
    
    var pressed : Bool = false
    
    func initialSetup() {
        self.trackColor = UIColor(red: 0.74, green: 0.74, blue: 0.74, alpha: 1.0)
        self.trackFillColor1 = UIColor(red: 1.0, green: 0.04, blue: 0.0, alpha: 1.0)
        self.trackFillColor2 = UIColor(red: 1.0, green: 0.37, blue: 0.0, alpha: 1.0)
        self.trackFillColor2pressed = UIColor(red: 0.7, green: 0.2, blue: 0.0, alpha: 1.0)
        
        trackLayer.toobaSlider = self
        trackLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(trackLayer)
        
        thumbLayer.toobaSlider = self
        thumbLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(thumbLayer)
        
        popView.addSubview(popupText)
        popupText.frame = popView.bounds
        popupText.textAlignment = NSTextAlignment.center
//        popView.backgroundColor = UIColor.redColor()
        
        updateLayerFrames()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateLayerFrames()
    }
    
    func positionPopup() {
        let y = self.frame.origin.y - popupDistance - popView.bounds.height
        var x = self.frame.origin.x + (self.bounds.width - self.bounds.height) * fillRatio() - popView.bounds.width / 2 + self.bounds.height / 2
        if x < self.frame.origin.x { x = self.frame.origin.x }
        if x + popView.frame.width > self.frame.origin.x + self.frame.width {
            x = self.frame.origin.x + self.frame.width - popView.frame.width
        }
        
        let r = CGRect(x: x, y: y, width: popupWidth, height: popupHeight)
        
        popView.frame = r
        popupText.frame = popView.bounds
        
        popupText.text = NSString(format: "%.2f", value) as String
    }
    
    func updateLayerFrames() {
        if showPopup { positionPopup() }
        
        thumbLayer.frame = bounds
        thumbLayer.setNeedsDisplay()

        trackLayer.frame = bounds //.insetBy(dx: 0, dy: bounds.height / 3)
        trackLayer.setNeedsDisplay()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        minValue = 0
        maxValue = 1
        value = 0.5
        minBound = 0.0
        
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        minValue = 0
        maxValue = 1
        value = 0.5
        minBound = 0.0
        
        initialSetup()
    }
    
    
    func thumbFrame() -> CGRect {
        let slider = self
        let persentage : CGFloat = CGFloat((slider.value - slider.minValue) / (slider.maxValue - slider.minValue))
        
        let x : CGFloat = CGFloat(slider.bounds.width - slider.bounds.height) * persentage
        let y : CGFloat = 0.0
        let w = slider.bounds.height
        let h = w

        return CGRect(x: x, y: y, width: w, height: h)
        
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        if showPopup {
            if let superview = self.superview {
                superview.addSubview(popView)
                positionPopup()
            }
        }
        
        previousLocation = touch.location(in: self)
        previousValue = value
        
        let r = thumbFrame().contains(previousLocation)
        
        pressed = true
        
        if !r {
            let trackLength = self.bounds.width - thumbFrame().width
            let touchLocation = touch.location(in: self).x - thumbFrame().width / 2
            var new_value = Double(touchLocation / trackLength)
            if new_value < 0 {
                new_value = 0
            }
            if new_value > 1 {
                new_value = 1
            }
            
            new_value *= maxValue - minValue
            
            value = new_value
            previousValue = value
            
            sendActions(for: UIControl.Event.valueChanged)
            
            print("TOUCH LOCATION: \(Double(new_value))")
            print(value)
        }
        
        updateLayerFrames()
        return true
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let location = touch.location(in: self)
        
        let deltaLocation = Double(location.x - previousLocation.x)
        let deltaValue = (maxValue - minValue) * deltaLocation / Double(bounds.width - thumbFrame().width)
        
        value = previousValue + deltaValue
        
        pressed = true
        
        updateLayerFrames()
        
        sendActions(for: UIControl.Event.valueChanged)
        
        return true
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        pressed = false
        popView.removeFromSuperview()
        updateLayerFrames()
    }
}
