//
//  SearchViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 29/10/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import UIKit

class SearchViewController : UIViewController, UITableViewDelegate, UISearchBarDelegate {
    @IBOutlet weak var tableView: ProjectTableViewWithoutPagination!
    var searchBar : UISearchBar!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDidShow(_ notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            let keyboardTop = keyboardRectangle.origin.y
            
            var activeCellNullable: ProjectTableViewCell? = nil
            
            for cell in tableView.visibleCells {
                if (cell as! ProjectTableViewCell).txtPay.isFirstResponder {
                    activeCellNullable = (cell as! ProjectTableViewCell)
                    break
                }
            }
            
            if let activeCell = activeCellNullable {
                let txtPay = activeCell.txtPay
                let p = (txtPay?.superview?.convert(txtPay!.frame.origin, to: self.tableView).y)! - self.tableView.contentOffset.y
                let diff = keyboardRectangle.origin.y - p - 64 - txtPay!.frame.height - 15
                
                if diff < 0 {
                    var cf = self.tableView.contentOffset
                    cf.y = cf.y - diff
                    if cf.y > 0 {
                        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                            self.tableView.contentOffset = cf
                        }, completion: nil)
                    }
                }
                
                let rect = activeCell.txtPay.frame.origin.y + activeCell.bounds.origin.y
            }
        }
    }
    
    @objc func keyboardDidHide(_ notification: NSNotification) {
        if tableView.contentSize.height < tableView.frame.height {
            return
        }
        let diff = tableView.contentSize.height - tableView.frame.height - tableView.contentOffset.y
        if diff < 0 {
            var cf = self.tableView.contentOffset
            cf.y = cf.y + diff
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                self.tableView.contentOffset = cf
            }, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)

        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor(red: 239/255, green: 243/255, blue: 245/255, alpha: 1.0)
        
        self.tableView.Scrolled = { [weak self] in
            guard let `self` = self else { return }

            self.searchBar.resignFirstResponder()
        }
        
        
        
        self.searchBar = UISearchBar()
        self.searchBar.sizeToFit()
        self.searchBar.placeholder = "PROJECT_SEARCH".localized
        
        self.searchBar.delegate = self
    
        self.searchBar.returnKeyType = .done
        self.searchBar.enablesReturnKeyAutomatically = false
        
        self.navigationItem.titleView = self.searchBar
        
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
        }
        
        
        self.tableView.RowSelected = { [unowned self] project, id in
            let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
            vc.projectInfo = project
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        tableView.SharePressed = { [unowned self] (project, index) in
            // text to share
            let beginning_text = "SHARE_TEXT".localized
            let text = "\(beginning_text) @\(project.tag) https://tooba.site/m/\(project.tag)"
            
            // set up activity view controller
            let objectsToShare: [AnyObject] = [ text as AnyObject ]
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
        
        tableView.PayPressed = { [unowned self] (project, index, amount) in
            if amount >= ProjectListViewController.silentPaymentLimit {
                Alerts.ShowActionSheet(sender: self, title: "DO_YOU_REALLY_WANT_TO_PAY".localized + " \(project.currency.toString(amount))?", items: ["YES".localized, "NO".localized], callback: { [weak self] choice in
                    if choice == 0 {
                        self?.showSelector(projectInfo: project, amount: amount)
                    }
                    }, completion: nil)
            } else {
                self.showSelector(projectInfo: project, amount: amount)
            }
        }
        
        if !Db.isAuthorized() {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func showSelector(projectInfo: Project, amount: Double) {
        self.view.showReloadView()
        Server.yandexKassaLinkedBankCards(successCompletion: { [unowned self] bankCardItems in
            self.view.hideReloadView()
            var bankCardMasks = bankCardItems.map({$0.maskWithAsterisks()})
            
            if bankCardItems.count == 0 {
                self.payAction(projectInfo: projectInfo, amount: amount)
                return
            }
            
            self.payAction(projectInfo: projectInfo, amount: amount, bankCardItems[0].cardMask)
            return
            }, onErrorCompletion: { [unowned self] in
                self.view.hideReloadView()
                Alerts.showErrorAlertWithOK(sender: self, title: "ERROR".localized, message: "CANT_MAKE_A_PAYMENT".localized, completion: nil)
        })
    }
    
    func payAction(projectInfo: Project, amount: Double, _ cardMask: String = "") {
        self.view.showReloadView()
        
        Server.yandexKassaPayment(projectId: projectInfo.id, amount: amount, cardMask: cardMask, successCompletion: { [unowned self] urlString in
            self.view.hideReloadView()
            
            if urlString == "Ok" {
                let vc = MotivationViewController.storyboardInstance()
                vc.projectInfo = projectInfo
                self.present(vc, animated: true, completion: { [unowned self] in
                    //                    self.silentReload()
                })
            } else {
                let url = URL(string: urlString)!
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.openURL(url);
                }
            }
            }, onErrorCompletion: { [unowned self] in
                self.view.hideReloadView()
                Alerts.showErrorAlertWithOK(sender: self, title: "Ошибка", message: "CANT_MAKE_A_PAYMENT".localized, completion: nil)
        })
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.searchBar.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchBar.resignFirstResponder()
    }


    var cancellationToken : CancellationToken? = nil

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        cancellationToken?()
        if searchText.isEmpty {
            self.tableView.setProjects(ps: [])
            return
        }
        self.cancellationToken = Server.SearchProjects(searchString: searchText, YES: { [unowned self] projects in
            self.tableView.setProjects(ps: projects)
        }, NO: { type, s in

        })
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.popViewController(animated: false)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        print(self.navigationController?.navigationBar.frame.height)
//    }
}
