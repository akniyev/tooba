//
//  UIToolbar.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 26/07/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import UIKit

extension UIToolbar {
    
    func ToolbarPicker(mySelect : Selector) -> UIToolbar {
        let TEXT_DONE = "Готово"
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: TEXT_DONE, style: UIBarButtonItem.Style.plain, target: self, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
}
