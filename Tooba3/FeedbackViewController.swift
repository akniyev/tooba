//
//  FeedbackViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 12/11/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class FeedbackViewController : UITableViewController {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var lblYourName: UILabel!
    @IBOutlet weak var lblYourFeedback: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    
    weak var feedbackVC2: FeedbackViewController2?
    
    @IBAction func nextPressed(_ sender: Any) {
        textField.becomeFirstResponder()
    }
    
    @IBAction func txtNameEditingBegin(_ sender: Any) {
//        self.tableView.scrollToRow(at: IndexPath(item: 1, section: 0), at: .top, animated: true)
//        self.tableView.scrollToRow(at: IndexPath.init(row: 1, section: 0), at: .top, animated: true)
    }
    
    func showFinalWindow() {
        let overlay = UIView(frame: self.view.bounds)
        let screenWidth = self.view.bounds.width
        let screenHeight = self.view.bounds.height
        let imageWidth = screenWidth / 1.5
        let imageFrame = CGRect(x: screenWidth / 4, y: screenHeight / 2 - screenWidth / 4, width: screenWidth / 2, height: screenWidth / 2)
        let imageView = UIImageView(frame: imageFrame)
        imageView.image = UIImage(named: "Feedback_FinalPicture")
        overlay.addSubview(imageView)
        self.view.addSubview(overlay)
    }
    
    public func SendFeedback() {
        textField.resignFirstResponder()
        txtName.resignFirstResponder()
        
        let name = txtName.text ?? ""
        let message = textField.text ?? ""
        let email = txtEmail.text ?? ""
        
        if (name.count > 0 && message.count > 0 && email.count > 0) {
            //            btnSendFeedback.isEnabled = false
            self.feedbackVC2?.btnSend.isEnabled = false
            txtName.isEnabled = false
            textField.isEditable = false
            Server.SendFeedback(name: name, message: message, email: email, YES: { [unowned self] in
                //                self.btnSendFeedback.isEnabled = true
                self.txtName.isEnabled = true
                self.textField.isEditable = true
                self.txtEmail.isEnabled = true
                self.feedbackVC2?.btnSend.isEnabled = true
                self.txtName.text = ""
                self.textField.text = ""
                self.txtEmail.text = ""
                self.performSegue(withIdentifier: "feedback_final", sender: self)
            }) { [unowned self] in
                //                self.btnSendFeedback.isEnabled = true
                self.txtName.isEnabled = true
                self.textField.isEditable = true
                self.txtEmail.isEnabled = true
                self.feedbackVC2?.btnSend.isEnabled = true
                Alerts.showErrorAlertWithOK(sender: self, title: "ERROR".localized, message: "CANT_SEND_YOUR_FEEDBACK".localized, completion: nil)
            }
        } else {
            Alerts.showErrorAlertWithOK(sender: self, title: "ERROR".localized, message: "MESSAGE_SHOULD_NOT_BE_EMPTY".localized, completion: nil)
        }
    }
    
    deinit {
//        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        lblYourName.text = "YOUR_NAME".localized.uppercased()
        lblYourFeedback.text = "YOUR_FEEDBACK".localized.uppercased()
        lblEmail.text = "YOUR_EMAIL".localized.uppercased()
//        btnSendFeedback.setTitle("SEND_FEEDBACK".localized.uppercased(), for: .normal)
        
//        NotificationCenter
//            .default
//            .addObserver(self, selector: #selector(keyboardDidShow(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
//        NotificationCenter
//            .default
//            .addObserver(self, selector: #selector(keyboardDidHide(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
//
        
        self.title = "FEEDBACK_TITLE".localized
        self.textField.layer.cornerRadius = 5.0
        self.textField.layer.borderWidth = 1.0
        self.textField.layer.borderColor = UIColor(displayP3Red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0).cgColor
        self.textField.backgroundColor = UIColor.white
        
        self.tableView.allowsSelection = false
    }
    
//    @objc func keyboardDidShow (_ notification: NSNotification) {
////        var frame = self.view.frame
////
////        if let userInfo = notification.userInfo {
////            if let keyboardSize = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
////                frame.origin.y = frame.origin.y - keyboardSize.height
////            } else {
////                frame.origin.y = self.y
////            }
////        } else {
////            frame.origin.y = self.y
////        }
////        self.view.frame = frame
//        if txtName.isFirstResponder {
//            self.tableView.scrollToRow(at: IndexPath.init(row: 1, section: 0), at: .top, animated: true)
//        }
//    }
//
//    @objc func keyboardDidHide (_ notification: NSNotification) {
////        var frame = self.view.frame
////        frame.origin.y = self.y
////        self.view.frame = frame
//    }
}
