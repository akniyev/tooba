//
// Created by Hasan Akniyev on 28/10/2017.
// Copyright (c) 2017 greenworld. All rights reserved.
//

import Foundation

class BankCardItem {
    var cardMask: String

    init(cardMask: String) {
        self.cardMask = cardMask
    }

    func maskWithAsterisks() -> String {
        return self.cardMask.replacingOccurrences(of: "|", with: "****")
    }
}
