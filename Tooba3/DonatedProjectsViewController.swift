//
//  DonatedProjectsViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 15/01/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit

class DonatedProjectsViewController : UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var tableView: PaymentHistoryProjectsTableView!
    var backView : BackgroundView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backView = BackgroundView(frame: self.view.bounds);
        self.view.addSubview(self.backView!);
        
        self.backView?.label.text = "PRESS_TO_RELOAD".localized
        self.backView?.isHidden = true
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.reloadTapped))
//        tap.delegate = self
//        self.backView?.addGestureRecognizer(tap)
        self.navigationItem.title = "DonatedProjectsViewController".localized
        
        tableView.RowSelected = { [unowned self] (p, i) in
            let vc = ProjectInfoWithButtonViewController.storyboardInstance()!
            vc.projectInfo = p
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.tableView.backgroundView = nil
        self.tableView.backgroundColor = UIColor(red: 238/255.0, green: 242/255.0, blue: 244/255.0, alpha: 1)
    }
    
//    @objc func reloadTapped() {
//        print("RELOAD")
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.parent?.title = "PROJECT_LIST_PROJECTS_TITLE".localized
        
        if Db.isAuthorized() {
            self.tableView.loadCurrentPages(forced: true)
        }
    }
}
