//
//  MenuViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 04/10/2018.
//  Copyright © 2018 greenworld. All rights reserved.
//

import UIKit
import SideMenu

class MenuViewController : UIViewController {
    @IBOutlet weak var btnDonations: UIButton!
    @IBOutlet weak var btnCards: UIButton!
    @IBOutlet weak var btnContacts: UIButton!
    @IBOutlet weak var btnFeedback: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    
    @IBAction func btnShareTouched(_ sender: Any) {
        // text to share
        let text = "https://itunes.apple.com/ru/app/tooba/id1247468713?l=en&mt=8"
        
        // set up activity view controller
        let objectsToShare: [AnyObject] = [ text as AnyObject ]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnDonations.setImage(UIImage(named: "MI_Donated_pressed"), for: .highlighted)
        btnCards.setImage(UIImage(named: "MI_Cards_pressed"), for: .highlighted)
        btnContacts.setImage(UIImage(named: "MI_Contacts_pressed"), for: .highlighted)
        btnFeedback.setImage(UIImage(named: "MI_Feedback_pressed"), for: .highlighted)
        
        btnDonations.setTitle("MENU_DONATIONS_TITLE".localized, for: .normal)
        btnCards.setTitle("MENU_MY_CARDS_TITLE".localized, for: .normal)
        btnContacts.setTitle("MENU_CONTACTS".localized, for: .normal)
        btnFeedback.setTitle("MENU_FEEDBACK".localized, for: .normal)
        btnShare.setTitle("MENU_SHARE_APP".localized, for: .normal)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        SideMenuManager.default.menuLeftNavigationController?.dismiss(animated: true, completion: nil)
    }
}
