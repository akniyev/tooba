//
//  String.swift
//  Tooba3
//
//  Created by Hasan on 27/07/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
