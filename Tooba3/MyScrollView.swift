//
//  MyScrollView.swift
//  Tooba3
//
//  Created by Admin on 15/10/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit

class MyScrollView : UIScrollView {
    var touched : (() -> ())? = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let t = touched {
            t()
        }
    }
}
