//
//  Server.swift
//  Tooba3
//
//  Created by Admin on 18/09/16.
//  Copyright © 2016 greenworld. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

typealias CancellationToken = () -> ()

class Server {
//    static let serverUrl : String = "http://dev.tooba.site"
    static let serverUrl : String = "http://tooba.site"
    static let protocolVersion = "3"
    static var navigationController : UINavigationController? = nil

    //This method is used to obtain new access_token when existing is expiring.
    //It checks if access_token is actual, if yes, then it call SUCCESS with this token
    //if no, then, if refresh_token is actual, it obtains new access_token. But if
    //refresh token is expired, then server signing out and user have to sign in again.
    static func RequestWithActualToken(SUCCESS: ((Credentials) -> ())?,
                                       ERROR: ((ErrorType, _ msg : String) -> ())?,
                                       UNAUTHORIZED : (() -> ())?) {
        if let c = Db.readCredentials() {
            //If user was authorized we should check if his tokes are expired or not
            if c.is_access_token_expired() {
                if c.is_refresh_token_expired() {
                    //If refresh token is expired we should sign out
                    UNAUTHORIZED?()
                    Server.SignOut()
                } else {
                    //Renew access token
                    Server.RenewAccessToken(YES: { rc in
                        SUCCESS?(rc)
                    }, NO: { et, msg in
                        if et == ErrorType.AuthorizationError {
                            UNAUTHORIZED?()
                            Server.SignOut()
                        } else {
                            ERROR?(et, msg)
                        }
                    })
                }
            } else {
                SUCCESS?(c)
            }
        } else {
            //If user is unauthorized, we call UNAUTHORIZED function
            UNAUTHORIZED?()
        }
    }
    
    static func SignUp(email: String, password: String, YES: (() -> ())?, NO: ((ErrorType, String) -> ())?) {
        let parameters : Parameters = [
            "email" : email,
            "password" : password,
        ]
        
        let headers : HTTPHeaders = [
            "Content-Type" : "application/json"
        ]
        
        let data = Alamofire.request(
                serverUrl + "/api/account/register",
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: headers).validate(statusCode: 201..<202)
        data.responseData(completionHandler: {
            response in
            switch response.result {
            case .success(let value):
                YES?()
            case .failure(let error):
                if response.response?.statusCode == 409 {
                    NO?(ErrorType.Conflict, "This email is already registered!")
                } else {
                    NO?(ErrorType.NetworkError, "Can't reach server!")
                }
            }
        })
    }

    static func SignUpWithoutRegistration(SUCCESS: ((TempUserData) -> ())?, ERROR: (() -> ())?) {
        if var ud = Db.readTempUserData() {
            ud.active = true
            SUCCESS?(ud)
            return
        }
        let parameters : Parameters = [:]

        let headers : HTTPHeaders = [:]

        let data = Alamofire.request(
                serverUrl + "/api/account/register/temp",
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: headers).validate(statusCode: 200..<201)
        data.responseData(completionHandler: {
            response in

            switch response.result {
            case .success(let value):
                let json = JSON(data: value)

                if json["username"].exists() && json["password"].exists() {
                    let username = json["username"].stringValue
                    let password = json["password"].stringValue

                    let ud = TempUserData(username: username, password: password)

                    Db.writeTempUserData(t: ud)

                    SUCCESS?(ud)
                } else {
                    ERROR?()
                }
            case .failure(let error):
                ERROR?()
            }
        })
    }
    
    static func SendFeedback(name: String, message: String, email: String, YES: (() -> ())?, NO: (() -> ())?) {
        let parameters : Parameters = [
            "client" : "ios",
            "name" : name,
            "message" : message,
            "contacts" : email,
        ]
        
        let headers : HTTPHeaders = [
            "Content-Type" : "application/x-www-form-urlencoded"
        ]
        
        let data = Alamofire.request(
            serverUrl + "/api/userresponse",
            method: .post,
            parameters: parameters,
            encoding: URLEncoding.default,
            headers: headers).validate(statusCode: 200..<201)
        
        data.responseData(completionHandler: {
            response in
            
            switch response.result {
            case .success(_):
                YES?()
            case .failure(_):
                NO?()
            }
        })
    }
    
    static func SetPersonalData(userInfo : UserInfo, YES: (()->())?, NO: (()->())?) {
        RequestWithActualToken(SUCCESS: { cred in
            let access_token = cred.access_token

            var dateString = ""
            if let bd = userInfo.birthday {
                let dayTimePeriodFormatter = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "dd.MM.yyyy"

                dateString = dayTimePeriodFormatter.string(from: bd)
            }
            let parameters : Parameters = [
                    "name" : userInfo.name ?? "",
                    "gender" : userInfo.gender?.toString() ?? "",
                    "birthday" : dateString
            ]
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "Authorization" : "Bearer \(access_token)"
            ]

            let data = Alamofire.request(serverUrl + "/api/account/info", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            data.responseData(completionHandler: {
                response in

                switch response.result {
                case .success(let value):
                    YES?()
                case .failure(let error):
                    NO?()
                }
            })
        }, ERROR: { et, msg in
            NO?()
        }, UNAUTHORIZED: {
            NO?()
        })
    }
    
    static func GetPersonalData(YES: ((UserInfo)->())?, NO: (()->())?) {
        RequestWithActualToken(SUCCESS: { cred in
            let access_token = cred.access_token

            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Bearer \(access_token)"
            ]

            let data = Alamofire.request(serverUrl + "/api/account/info", method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            data.responseJSON(completionHandler: {
                response in

                switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    var userInfo = UserInfo()
                    userInfo.name = json["name"].string
                    userInfo.gender = Gender.fromString(s: json["gender"].string)
                    if let dateString = json["birthday"].string {
                        var parts = dateString.characters.split(separator: ".").map(String.init)

                        if let d = Int(parts[0]) {
                            if let m = Int(parts[1]) {
                                if let y = Int(parts[2]) {
                                    userInfo.setBirthdate(day: d, month: m, year: y)
                                }
                            }
                        }
                    }

                    let isEmailConfirmed : Bool = json["isEmailConfirmed"].bool ?? false
                    userInfo.isEmailConfirmed = isEmailConfirmed

                    YES?(userInfo)
                case .failure(let error):
                    NO?()
                }
            })
        }, ERROR: { et, msg in
            NO?()
        }, UNAUTHORIZED: {
            NO?()
        })

        Server.RenewAccessToken(YES: { cred in
            if let c = Db.readCredentials() {


                
            } else {
                if let no = NO {
                    no()
                }
            }
        }, NO: { et, msg in
            if let no = NO {
                no()
            }
        })
    }

    //In some cases if access_token have small lifetime but nevertheless was expired,
    //it seems reasonable to invalidate this access_token and get new one.
    //These cases are getProjectList
    static func InvalidateAccessToken() {
        if let c = Db.readCredentials() {
            c.set_access_token_created_from_unixtime(unixtime: 0)
            Db.writeCredentials(c: c)
        }
    }
    
    static func SendFirebaseToken(token: String, YES: (()->())?, NO: (()->())?) {
        let urlString = "\(serverUrl)/api/account/firebase/token"
        
        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = ["token":token]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(cred.access_token)",
            ]
            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            
            data.response(completionHandler: { response in
                if response.response?.statusCode == 200 {
                    YES?()
                } else {
                    NO?()
                }
            })
        }, ERROR: { (errorType, msg) in
            NO?()
        }, UNAUTHORIZED: { () in
            NO?()
        })
    }
    
    static func getProjectInfo(projectId: Int, successCompletion: ((Project) -> ())?, onErrorCompletion: (() -> ())?) {
        let urlString = "\(serverUrl)/api/projects/\(projectId)?v=\(protocolVersion)"

        RequestWithActualToken(SUCCESS: { c in
            
            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(c.access_token)",
            ]
            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            
            data.responseJSON(completionHandler: {
                response in
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    let projectInfo = Project()
                    projectInfo.id = json["id"].intValue
                    projectInfo.name = json["name"].stringValue
                    projectInfo.tag = json["tag"].stringValue.trimmingCharacters(in: ["@"])
                    projectInfo.description = json["description"].stringValue
                    projectInfo.short_description = json["shortDescription"].stringValue
                    projectInfo.goal = json["goal"].double!
                    projectInfo.reached = json["reached"].double!
                    projectInfo.currency = Currency.fromString(s: json["currencyName"].stringValue)
                    projectInfo.hasSameTagFinishedProjects = json["hasSameTagFinishedProjects"].boolValue
                    projectInfo.fundName = json["fund"]["name"].stringValue
                    projectInfo.likeCount = json["likesCount"].intValue
                    projectInfo.donatedCount = json["donatorsCount"].intValue
                    projectInfo.isLiked = json["isLiked"].boolValue
                    
                    var images : [ImageInfo] = []
                    
                    if json["media"].exists() {
                        for (_, mediaItem) : (String, JSON) in json["media"] {
                            let kind = mediaItem["kind"].stringValue.lowercased()
                            if kind == "image" {
                                let newItem = ImageInfo(bigImageUrl: mediaItem["url"].stringValue, smallImageUrl: mediaItem["urlSmall"].stringValue)
                                images.append(newItem)
                            } else if kind == "youtube" {
                                let newItem = ImageInfo(youtubeCode: mediaItem["code"].stringValue)
                                images.append(newItem)
                            }
                        }
                    }
                    
//                    if json["mainImageUrl"].exists() && json["mainImageUrlSmall"].exists() {
//                        images.append(ImageInfo(bigImageUrl: json["mainImageUrl"].stringValue, smallImageUrl: json["mainImageUrlSmall"].stringValue))
//                    }
//
//                    if json["otherImagesUrl"].exists() && json["otherImagesUrlSmall"].exists() {
//                        let a1 = json["otherImagesUrl"].arrayObject!.map({ x in return x as! String })
//                        let a2 = json["otherImagesUrlSmall"].arrayObject!.map({ x in return x as! String })
//
//                        let a = zip(a1, a2).map({ x in return ImageInfo(bigImageUrl: x.0, smallImageUrl: x.1)})
//
//                        images.append(contentsOf: a)
//                    }
                    
                    projectInfo.photos = images

                    successCompletion?(projectInfo)
                    
                case .failure(let _):
                    Server.InvalidateAccessToken()
                    onErrorCompletion?()
                }
            })
            
        }, ERROR: { et, msg in
            onErrorCompletion?()
        }, UNAUTHORIZED: {
            onErrorCompletion?()
        })
    }
    
    
    
    static func getProjectInfo(projectTag: String, successCompletion: ((Project) -> ())?, NO: (() -> ())?) {
        let urlString = "\(serverUrl)/api/projects/tag/@\(projectTag)"
        
        RequestWithActualToken(SUCCESS: { c in
            
            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(c.access_token)",
            ]
            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            
            data.responseJSON(completionHandler: {
                response in
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    let projectInfo = Project()
                    projectInfo.id = json["id"].intValue
                    projectInfo.name = json["name"].stringValue
                    projectInfo.tag = json["tag"].stringValue.trimmingCharacters(in: ["@"])
                    projectInfo.description = json["description"].stringValue
                    projectInfo.short_description = json["shortDescription"].stringValue
                    projectInfo.goal = json["goal"].double!
                    projectInfo.reached = json["reached"].double!
                    projectInfo.currency = Currency.fromString(s: json["currencyName"].stringValue)
                    projectInfo.hasSameTagFinishedProjects = json["hasSameTagFinishedProjects"].boolValue
                    projectInfo.fundName = json["fund"]["name"].stringValue
                    projectInfo.likeCount = json["likesCount"].intValue
                    projectInfo.donatedCount = json["donatorsCount"].intValue
                    projectInfo.isLiked = json["isLiked"].boolValue
                    
                    var images : [ImageInfo] = []
                    
                    if json["mainImageUrl"].exists() && json["mainImageUrlSmall"].exists() {
                        images.append(ImageInfo(bigImageUrl: json["mainImageUrl"].stringValue, smallImageUrl: json["mainImageUrlSmall"].stringValue))
                    }
                    
                    if json["otherImagesUrl"].exists() && json["otherImagesUrlSmall"].exists() {
                        let a1 = json["otherImagesUrl"].arrayObject!.map({ x in return x as! String })
                        let a2 = json["otherImagesUrlSmall"].arrayObject!.map({ x in return x as! String })
                        
                        let a = zip(a1, a2).map({ x in return ImageInfo(bigImageUrl: x.0, smallImageUrl: x.1)})
                        
                        images.append(contentsOf: a)
                    }
                    
                    projectInfo.photos = images
                    
                    successCompletion?(projectInfo)
                    
                case .failure(let _):
                    Server.InvalidateAccessToken()
                    NO?()
                }
            })
            
        }, ERROR: { et, msg in
            NO?()
        }, UNAUTHORIZED: {
            NO?()
        })
    }

    static func GetProjectList(listType : ProjectListType, itemsOnPage : Int, pageNumber : Int, YES: (([Project]) -> ())?, NO: ((ErrorType, String) -> ())?) {
//        var urlString = ""
//        switch listType {
//        case .active:
//            urlString = "\(serverUrl)/api/projects/page/\(pageNumber)/pagesize/\(itemsOnPage)"
//        case .Favorites:
//            urlString = "\(serverUrl)/api/projects/user/page/\(pageNumber)/pagesize/\(itemsOnPage)"
//        case .finished:
//            urlString = "\(serverUrl)/api/projects/user/page/\(pageNumber)/pagesize/\(itemsOnPage)"
//        }
        
        let urlString = "\(serverUrl)/api/projects?v=\(protocolVersion)&page=\(pageNumber)&pagesize=\(itemsOnPage)"
        
        RequestWithActualToken(SUCCESS: { c in

            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Bearer \(c.access_token)",
            ]
            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)

            data.responseJSON(completionHandler: {
                response in

                switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    var result : [Project] = []

                    let projectsJson = json["projects"]
                    
                    for (_,sj):(String,JSON) in projectsJson {
                        let p = Project()
                        p.id = sj["id"].intValue
                        p.name = sj["name"].stringValue
                        p.tag = sj["tag"].stringValue.trimmingCharacters(in: ["@"])
                        p.description = sj["description"].stringValue
                        p.short_description = sj["shortDescription"].stringValue
                        p.goal = sj["goal"].double!
                        p.reached = sj["reached"].double!
                        p.currency = Currency.fromString(s: sj["currency"].stringValue)
                        p.fundName = sj["fund"]["name"].stringValue
                        p.likeCount = sj["likesCount"].intValue
                        p.donatedCount = sj["donatorsCount"].intValue
                        p.isLiked = sj["isLiked"].boolValue
                        p.hasSameTagFinishedProjects = sj["hasSameTagFinishedProjects"].boolValue

                        var images : [ImageInfo] = []
                        
                        if sj["media"].exists() {
                            for (_, mediaItem) : (String, JSON) in sj["media"] {
                                let kind = mediaItem["kind"].stringValue.lowercased()
                                if kind == "image" {
                                    let newItem = ImageInfo(bigImageUrl: mediaItem["url"].stringValue, smallImageUrl: mediaItem["urlSmall"].stringValue)
                                    images.append(newItem)
                                } else if kind == "youtube" {
                                    let newItem = ImageInfo(youtubeCode: mediaItem["code"].stringValue)
                                    images.append(newItem)
                                }
                            }
                        }
                        
//                        if sj["mainImageUrl"].exists() && sj["mainImageUrlSmall"].exists() {
//                            images.append(ImageInfo(bigImageUrl: sj["mainImageUrl"].stringValue, smallImageUrl: sj["mainImageUrlSmall"].stringValue))
//                        }
//
//                        if sj["otherImagesUrl"].exists() && sj["otherImagesUrlSmall"].exists() {
//                            let a1 = sj["otherImagesUrl"].arrayObject!.map({ x in return x as! String })
//                            let a2 = sj["otherImagesUrlSmall"].arrayObject!.map({ x in return x as! String })
//
//                            let a = zip(a1, a2).map({ x in return ImageInfo(bigImageUrl: x.0, smallImageUrl: x.1)})
//
//                            images.append(contentsOf: a)
//                        }

                        p.photos = images

                        result.append(p)
                    }
                    YES?(result)

                case .failure(let e):
                    Server.InvalidateAccessToken()
                    NO?(ErrorType.NetworkError, e.localizedDescription)
                }
            })

        }, ERROR: { et, msg in
            NO?(et, msg)
        }, UNAUTHORIZED: {
            NO?(ErrorType.Unauthorized, "Authorization failed")
        })
    }

    static func GetDonationHistory(itemsOnPage : Int, pageNumber : Int, YES: (([Project]) -> ())?, NO: ((ErrorType, String) -> ())?) {
        var urlString = "\(serverUrl)/api/projects/donated?page=\(pageNumber)&pagesize=\(itemsOnPage)"

        RequestWithActualToken(SUCCESS: { c in

            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(c.access_token)",
            ]
            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)

            data.responseJSON(completionHandler: {
                response in

                switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    var result : [Project] = []

                    for (_,_sj):(String,JSON) in json {
                        let p = Project()
                        let sj = _sj["project"]
                        p.id = sj["id"].intValue
                        p.name = sj["name"].stringValue
                        p.tag = sj["tag"].stringValue.trimmingCharacters(in: ["@"])
                        p.description = sj["description"].stringValue
                        p.short_description = sj["shortDescription"].stringValue
                        p.goal = sj["goal"].double!
                        p.reached = sj["reached"].double!
                        p.currency = Currency.fromString(s: sj["currencyName"].stringValue)
                        p.donatedByMe = _sj["donated"].doubleValue

                        p.likeCount = sj["likesCount"].intValue
                        p.donatedCount = sj["donatorsCount"].intValue
                        p.isLiked = sj["isLiked"].boolValue

                        var images : [ImageInfo] = []

                        if sj["mainImageUrl"].exists() && sj["mainImageUrlSmall"].exists() {
                            images.append(ImageInfo(bigImageUrl: sj["mainImageUrl"].stringValue, smallImageUrl: sj["mainImageUrlSmall"].stringValue))
                        }

                        if sj["otherImagesUrl"].exists() && sj["otherImagesUrlSmall"].exists() {
                            let a1 = sj["otherImagesUrl"].arrayObject!.map({ x in return x as! String })
                            let a2 = sj["otherImagesUrlSmall"].arrayObject!.map({ x in return x as! String })

                            let a = zip(a1, a2).map({ x in return ImageInfo(bigImageUrl: x.0, smallImageUrl: x.1)})

                            images.append(contentsOf: a)
                        }

                        p.photos = images

                        result.append(p)
                    }
                    YES?(result)

                case .failure(let e):
                    Server.InvalidateAccessToken()
                    NO?(ErrorType.NetworkError, e.localizedDescription)
                }
            })

        }, ERROR: { et, msg in
            NO?(et, msg)
        }, UNAUTHORIZED: {
            NO?(ErrorType.Unauthorized, "Authorization failed")
        })
    }
    
    static func GetFinishedProjects(tag: String, itemsOnPage : Int, pageNumber : Int, YES: (([Project]) -> ())?, NO: ((ErrorType, String) -> ())?) {
        var urlString = "\(serverUrl)/api/projects/finished/tag/\(tag)?page=\(pageNumber)&pagesize=\(itemsOnPage)"
        
        RequestWithActualToken(SUCCESS: { c in
            
            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(c.access_token)",
            ]
            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            
            data.responseJSON(completionHandler: {
                response in
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    var result : [Project] = []
                    
                    for (_,sj):(String,JSON) in json {
                        let p = Project()
                        p.id = sj["id"].intValue
                        p.name = sj["name"].stringValue
                        p.tag = sj["tag"].stringValue.trimmingCharacters(in: ["@"])
                        p.description = sj["description"].stringValue
                        p.short_description = sj["shortDescription"].stringValue
                        p.goal = sj["goal"].double!
                        p.reached = sj["reached"].double!
                        p.currency = Currency.fromString(s: sj["currencyName"].stringValue)
//                        p.donatedByMe = sj["donated"].doubleValue
                        
                        p.likeCount = sj["likesCount"].intValue
                        p.donatedCount = sj["donatorsCount"].intValue
                        p.isLiked = sj["isLiked"].boolValue
                        p.hasSameTagFinishedProjects = sj["hasSameTagFinishedProjects"].boolValue
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        let dateText = sj["reachedDate"].stringValue.split(separator: ".")[0]
                        p.finishedDate = dateFormatter.date(from: String(dateText))
                        
                        var images : [ImageInfo] = []
                        
                        if sj["mainImageUrl"].exists() && sj["mainImageUrlSmall"].exists() {
                            images.append(ImageInfo(bigImageUrl: sj["mainImageUrl"].stringValue, smallImageUrl: sj["mainImageUrlSmall"].stringValue))
                        }
                        
                        if sj["otherImagesUrl"].exists() && sj["otherImagesUrlSmall"].exists() {
                            let a1 = sj["otherImagesUrl"].arrayObject!.map({ x in return x as! String })
                            let a2 = sj["otherImagesUrlSmall"].arrayObject!.map({ x in return x as! String })
                            
                            let a = zip(a1, a2).map({ x in return ImageInfo(bigImageUrl: x.0, smallImageUrl: x.1)})
                            
                            images.append(contentsOf: a)
                        }
                        
                        p.photos = images
                        
                        result.append(p)
                    }
                    YES?(result)
                    
                case .failure(let e):
                    Server.InvalidateAccessToken()
                    NO?(ErrorType.NetworkError, e.localizedDescription)
                }
            })
            
        }, ERROR: { et, msg in
            NO?(et, msg)
        }, UNAUTHORIZED: {
            NO?(ErrorType.Unauthorized, "Authorization failed")
        })
    }

    static func SearchProjects(searchString: String, YES: (([Project]) -> ())?, NO: ((ErrorType, String) -> ())?) -> CancellationToken {
        var cancelled = false
        let urlString = "\(serverUrl)/api/projects/search?query=\(searchString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
        print(urlString)
        RequestWithActualToken(SUCCESS: { c in

            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(c.access_token)",
            ]
            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)

            data.responseJSON(completionHandler: {
                response in

                if cancelled {
                    NO?(ErrorType.Unknown, "Cancelled")
                    return
                }

                switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    var result : [Project] = []

                    for (_,sj):(String,JSON) in json["projects"] {
                        let p = Project()
                        p.id = sj["id"].intValue
                        p.name = sj["name"].stringValue
                        p.tag = sj["tag"].stringValue.trimmingCharacters(in: ["@"])
                        p.description = sj["description"].stringValue
                        p.short_description = sj["shortDescription"].stringValue
                        p.goal = sj["goal"].double!
                        p.reached = sj["reached"].double!
                        p.currency = Currency.fromString(s: sj["currencyName"].stringValue)
                        p.hasSameTagFinishedProjects = sj["hasSameTagFinishedProjects"].boolValue
                        p.fundName = sj["fund"]["name"].stringValue
                        
                        p.likeCount = sj["likesCount"].intValue
                        p.donatedCount = sj["donatorsCount"].intValue
                        p.isLiked = sj["isLiked"].boolValue

                        var images : [ImageInfo] = []

                        if sj["mainImageUrl"].exists() && sj["mainImageUrlSmall"].exists() {
                            images.append(ImageInfo(bigImageUrl: sj["mainImageUrl"].stringValue, smallImageUrl: sj["mainImageUrlSmall"].stringValue))
                        }

                        if sj["otherImagesUrl"].exists() && sj["otherImagesUrlSmall"].exists() {
                            let a1 = sj["otherImagesUrl"].arrayObject!.map({ x in return x as! String })
                            let a2 = sj["otherImagesUrlSmall"].arrayObject!.map({ x in return x as! String })

                            let a = zip(a1, a2).map({ x in return ImageInfo(bigImageUrl: x.0, smallImageUrl: x.1)})

                            images.append(contentsOf: a)
                        }

                        p.photos = images

                        result.append(p)
                    }
                    YES?(result)

                case .failure(let e):
                    Server.InvalidateAccessToken()
                    NO?(ErrorType.NetworkError, e.localizedDescription)
                }
            })

        }, ERROR: { et, msg in
            NO?(et, msg)
        }, UNAUTHORIZED: {
            NO?(ErrorType.Unauthorized, "Authorization failed")
        })

        return { cancelled = true }
    }

    static func ResetPassword(email: String, YES: (() -> ())?, ERROR: ((String) -> ())?) {
        let parameters : Parameters = [:]
        let headers : HTTPHeaders = [:]
        let emailEncoded = email.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let urlString = "\(Server.serverUrl)/api/account/forgot/" + emailEncoded
        
        let url : URLConvertible = urlString
        let data = Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers)
        
        data.responseJSON(completionHandler: {
            response in
            
            switch response.result {
            case .success(let _):
                if response.response?.statusCode == 204 {
                    if let yes = YES {
                        yes()
                    }
                } else {
                    if let no = ERROR {
                        no("Server error")
                    }
                }
            case .failure(let value):
                if let no = ERROR {
                    no(value.localizedDescription)
                }
            }
        })
        if let yes = YES {
            yes()
        }
    }
    
    static func AddProjectToFavorites(id: Int, YES: (() -> ())?, NO : ((String) -> ())?) {
        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Bearer \(cred.access_token)",
            ]
            let urlString = "\(Server.serverUrl)/api/projects/\(id)/like"

            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: headers)

            data.responseJSON(completionHandler: {
                response in

                switch response.result {
                case .success(let _):
                    if response.response?.statusCode == 204 {
                        YES?()
                    } else {
                        NO?("Server error")
                    }
                case .failure(let value):
                    NO?(value.localizedDescription)
                }
            })
        }, ERROR: { et, msg in
            NO?(msg)
        }, UNAUTHORIZED: {
            NO?("Unauthorized")
        })
    }
    
    static func RemoveProjectFromFavorites(id: Int, YES: (() -> ())?, NO : ((String) -> ())?) {
        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Bearer \(cred.access_token)",
            ]
            let urlString = "\(Server.serverUrl)/api/projects/\(id)/dislike"

            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            data.responseJSON(completionHandler: {
                response in

                switch response.result {
                case .success(let _):
                    if response.response?.statusCode == 204 {
                        YES?()
                    } else {
                        NO?("Server error")
                    }
                case .failure(let value):
                    NO?(value.localizedDescription)
                }
            })
        }, ERROR: { et, msg in
            NO?(msg)
        }, UNAUTHORIZED: {
            NO?("Unauthorized")
        })
    }
    
    static func SignOut() {
        Db.writeCredentials(c: nil)
        if let nc = navigationController {
            nc.popToRootViewController(animated: true)
        }
    }
    
    static func ResendConfirmationEmail(YES: (() -> ())?, NO: ((ErrorType, String) -> ())?) {
        RequestWithActualToken(SUCCESS: { c in
            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Bearer \(c.access_token)",
            ]
            let urlString = "\(Server.serverUrl)/api/account/confirm"

            let url : URLConvertible = urlString
            let data = Alamofire.request(
                    url,
                    method: .post,
                    parameters: parameters,
                    encoding: URLEncoding.default,
                    headers: headers).validate(statusCode: 200..<201)
            data.responseJSON(completionHandler: {
                response in
                switch response.result {
                case .success(let _):
                    YES?()
                case .failure(let e):
                    if response.response?.statusCode == 401 {
                        NO?(ErrorType.Unauthorized, "Authorization error")
                    } else {
                        NO?(ErrorType.NetworkError, e.localizedDescription)
                    }
                }
            })
        }, ERROR: { et, msg in
            NO?(et, msg)
        }, UNAUTHORIZED: {
            NO?(ErrorType.Unauthorized, "Authorization error")
        })
    }

    static func SignInWithEmail(email: String, password: String, YES: ((Credentials) -> ())?, NO: ((ErrorType, String) -> ())?) {
        var parameters : Parameters = [
                "username" : email,
                "grant_type" : "password",
                "scope" : "openid offline_access",
                "password" : password
        ]
        if let ud = Db.readTempUserData() {
            if ud.active {
                parameters["tempusername"] = ud.username
                parameters["temppassword"] = ud.password
            }
        }

        let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded"
        ]
        let data = Alamofire.request(
                serverUrl + "/api/connect/token",
                method: .post,
                parameters: parameters,
                encoding: URLEncoding.default,
                headers: headers).validate(statusCode: 200..<201)
        data.responseJSON(completionHandler: {
            response in

            switch response.result {
            case .success(let value):
                let json = JSON(value)

                if json["access_token"].exists() && json["refresh_token"].exists() {
                    let c = Credentials()
                    c.access_token = json["access_token"].stringValue
                    c.access_token_created = Date()
                    c.refresh_token = json["refresh_token"].stringValue
                    c.refresh_token_created = Date()
                    c.email = email

                    if Db.writeCredentials(c: c) {
                        Db.deactivateTempUser()
                        YES?(c)
                    } else {
                        NO?(ErrorType.DatabaseError, "Can't write to database")
                    }
                } else {
                    NO?(ErrorType.InvalidData, "Received invalid data from server!")
                }
            case .failure(let error):
                if response.response?.statusCode == 400 {
                    NO?(ErrorType.InvalidCredentials, "Invalid credentials!")
                } else {
                    NO?(ErrorType.NetworkError, "Network problem. Can't reach server!")
                }
            }
        })
    }

    static func SignInAsTempUser(username: String, password: String, YES: ((Credentials) -> ())?, NO: ((ErrorType, String) -> ())?) {
        let parameters : Parameters = [
            "username" : username,
            "grant_type" : "password",
            "scope" : "openid offline_access",
            "password" : password
        ]
        let headers : HTTPHeaders = [
            "Content-Type" : "application/x-www-form-urlencoded"
        ]
        let data = Alamofire.request(
                serverUrl + "/api/connect/token",
                method: .post,
                parameters: parameters,
                encoding: URLEncoding.default,
                headers: headers).validate(statusCode: 200..<201)
        data.responseJSON(completionHandler: {
            response in

            switch response.result {
            case .success(let value):
                let json = JSON(value)

                if json["access_token"].exists() && json["refresh_token"].exists() {
                    let c = Credentials()
                    c.access_token = json["access_token"].stringValue
                    c.access_token_created = Date()
                    c.refresh_token = json["refresh_token"].stringValue
                    c.refresh_token_created = Date()
                    c.email = username
                    c.loginType = .Temp

                    if Db.writeCredentials(c: c) {
                        Db.activateTempUser()
                        YES?(c)
                    } else {
                        NO?(ErrorType.DatabaseError, "Can't write to database")
                    }
                } else {
                    NO?(ErrorType.InvalidData, "Received invalid data from server!")
                }
            case .failure(let error):
                if response.response?.statusCode == 400 {
                    NO?(ErrorType.InvalidCredentials, "Invalid credentials!")
                } else {
                    NO?(ErrorType.NetworkError, "Network problem. Can't reach server!")
                }
            }
        })
    }

    static func SignInWithFacebook(token: String, YES: ((Credentials) -> ())?, NO: ((ErrorType, String) -> ())?) {
        var parameters : Parameters = [
            "identity_provider" : "facebook",
            "access_token" : token,
            "grant_type" : "external",
            "scope" : "openid offline_access"
        ]
        if let ud = Db.readTempUserData() {
            if ud.active {
                parameters["tempusername"] = ud.username
                parameters["temppassword"] = ud.password
            }
        }
        let headers : HTTPHeaders = [
            "Content-Type" : "application/x-www-form-urlencoded"
        ]
        let data = Alamofire.request(
                serverUrl + "/api/connect/token",
                method: .post,
                parameters: parameters,
                encoding: URLEncoding.default,
                headers: headers).validate(statusCode: 200..<201)
        data.responseJSON(completionHandler: {
            response in

            switch response.result {
            case .success(let value):
                let json = JSON(value)

                if json["access_token"].exists() && json["refresh_token"].exists() {
                    let c = Credentials()
                    c.access_token = json["access_token"].stringValue
                    c.access_token_created = Date()
                    c.refresh_token = json["refresh_token"].stringValue
                    c.refresh_token_created = Date()
                    c.email = ""
                    c.loginType = .Facebook
                    c.social_token = token

                    if Db.writeCredentials(c: c) {
                        Db.deactivateTempUser()
                        YES?(c)
                    } else {
                        NO?(ErrorType.DatabaseError, "Database error")
                    }
                } else {
                    NO?(ErrorType.InvalidData, "Received invalid data from server!")
                }
            case .failure(let e):
                if response.response?.statusCode == 400 {
                    NO?(ErrorType.InvalidCredentials, "Can't get user information from Facebook!")
                } else {
                    NO?(ErrorType.NetworkError, "Can't reach server!")
                }
            }
        })
    }

    static func SignInWithGoogle(token: String, YES: ((Credentials) -> ())?, NO: ((ErrorType, String) -> ())?) {
        var parameters : Parameters = [
            "identity_provider" : "google",
            "access_token" : token,
            "grant_type" : "external",
            "scope" : "openid offline_access"
        ]
        if let ud = Db.readTempUserData() {
            if ud.active {
                parameters["tempusername"] = ud.username
                parameters["temppassword"] = ud.password
            }
        }
        let headers : HTTPHeaders = [
            "Content-Type" : "application/x-www-form-urlencoded"
        ]
        let data = Alamofire.request(
                serverUrl + "/api/connect/token",
                method: .post,
                parameters: parameters,
                encoding: URLEncoding.default,
                headers: headers).validate(statusCode: 200..<201)
        data.responseJSON(completionHandler: {
            response in

            switch response.result {
            case .success(let value):
                let json = JSON(value)

                if json["access_token"].exists() && json["refresh_token"].exists() {
                    let c = Credentials()
                    c.access_token = json["access_token"].stringValue
                    c.access_token_created = Date()
                    c.refresh_token = json["refresh_token"].stringValue
                    c.refresh_token_created = Date()
                    c.email = ""
                    c.loginType = .Facebook
                    c.social_token = token

                    if Db.writeCredentials(c: c) {
                        Db.deactivateTempUser()
                        YES?(c)
                    } else {
                        NO?(ErrorType.DatabaseError, "Database error")
                    }
                } else {
                    NO?(ErrorType.InvalidData, "Received invalid data from server!")
                }
            case .failure(let e):
                if response.response?.statusCode == 400 {
                    NO?(ErrorType.InvalidCredentials, "Can't get user information from Google!")
                } else {
                    NO?(ErrorType.NetworkError, "Can't reach server!")
                }
            }
        })
    }

    static func RenewAccessToken(YES: ((Credentials) -> ())?, NO: ((_ type : ErrorType, _ msg : String) -> ())?) {
        //Nullable credentials
        let cq = Db.readCredentials()

        //If cq nil than we are unauthorized
        if cq == nil {
            NO?(ErrorType.Unauthorized, "Unauthorized")
            return
        }
        let c = cq!

        let parameters : Parameters = [
                "refresh_token" : c.refresh_token,
                "grant_type" : "refresh_token",
                "scope" : "openid offline_access"
        ]
        let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded"
        ]
        let url : URLConvertible = "\(serverUrl)/api/connect/token"
        let data = Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers)

        data.responseJSON(completionHandler: {
            response in

            switch response.result {
            case .success(let value):
                let json = JSON(value)

                if json["access_token"].exists() && json["refresh_token"].exists() {
                    var c = Credentials()

                    if let cred = Db.readCredentials() {
                        c.email = cred.email
                        c.loginType = cred.loginType
                        c.social_token = cred.social_token
                    }

                    c.access_token = json["access_token"].stringValue
                    c.access_token_created = Date()
                    c.refresh_token = json["refresh_token"].stringValue
                    c.refresh_token_created = Date()

                    if Db.writeCredentials(c: c) {
                        YES?(c)
                    } else {
                        NO?(ErrorType.DatabaseError, "Can't write to database")
                    }
                } else if json["error"].exists() && json["error_description"].exists() {
                    NO?(ErrorType.AuthorizationError, json["error_description"].stringValue)
                } else {
                    NO?(ErrorType.InvalidData, "Invalid data")
                }
            case .failure(let error):
                NO?(ErrorType.NetworkError, error.localizedDescription)
            }
        })
    }

    static func ChangePassword(oldPasswordPlain : String, newPasswordPlain : String, SUCCESS : (() -> ())?, NO: (() -> ())?) {
        if (oldPasswordPlain.characters.count == 0) || (newPasswordPlain.characters.count == 0) {
            NO?()
            return
        }

        let oldPasswordEncoded = oldPasswordPlain.addingPercentEncoding(withAllowedCharacters: [])!
        let newPasswordEncoded = newPasswordPlain.addingPercentEncoding(withAllowedCharacters: [])!

        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Bearer \(cred.access_token)",
            ]
            let urlString = "\(Server.serverUrl)/api/account/password/\(oldPasswordEncoded)/\(newPasswordEncoded)"

            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: headers)

            data.responseJSON(completionHandler: {
                response in

                if response.response?.statusCode == 200 {
                    SUCCESS?()
                } else {
                    NO?()
                }
            })
        }, ERROR: { et, msg in
            NO?()
        }, UNAUTHORIZED: {
            NO?()
        })
    }

    //Make this with actual request to server
    static func GetAvailablePaymentSystems(projectId : Int, YES: (([PaymentSystem])->())?, NO: (()->())?) {
        let availablePaymentSystems = [PaymentSystem.WebMoney]
        YES?(availablePaymentSystems)
    }

    static func GetWebMoneyTransactionId(projectId : Int, amount : Double, currency : Currency, YES: ((String, Double, Currency)->())?, NO: ((String)->())?) {
        let currencyId = currency.getServerCode()
        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = [
                    "projectId" : projectId,
                    "amount" : amount,
                    "currency" : currencyId
            ]
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Bearer \(cred.access_token)",
                    "Cache-Control": "no-cache"
            ]
            let urlString = "\(Server.serverUrl)/api/payments/webmoney"

            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            data.responseData(completionHandler: {
                response in

                switch response.result {
                case .success(let _):
                    if response.response?.statusCode == 200 {
                        String()
                        if let transactionId = String(data: response.data!, encoding: .utf8) {
                            YES?(transactionId, amount, currency)
                        } else {
                            NO?("Wrong response")
                        }
                    } else {
                        NO?("Server error")
                    }
                case .failure(let value):
                    NO?(value.localizedDescription)
                }
            })
        }, ERROR: { et, msg in
            NO?(msg)
        }, UNAUTHORIZED: {
            NO?("Unauthorized")
        })
    }

    static func WebmoneyPaymentNotification(transactionId : String, amount : Double, currency : Currency,
                                            YES: (()->())?, NO: ((String)->())?) {
        let currencyId = currency.getServerCode()
        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = [
                    "transactionId" : transactionId,
                    "amount" : amount,
                    "currency" : currencyId
            ]
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Bearer \(cred.access_token)",
                    "Cache-Control": "no-cache"
            ]
            let urlString = "\(Server.serverUrl)/api/paymentshandler/webmoney"

            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            data.responseData(completionHandler: {
                response in

                switch response.result {
                case .success(let _):
                    if response.response?.statusCode == 200 {
                        YES?()
                    } else {
                        NO?("Server error")
                    }
                case .failure(let value):
                    NO?(value.localizedDescription)
                }
            })
        }, ERROR: { et, msg in
            NO?(msg)
        }, UNAUTHORIZED: {
            NO?("Unauthorized")
        })
    }

    static func yandexKassaPayment(projectId: Int, amount: Double, cardMask: String, successCompletion: ((String) -> ())?, onErrorCompletion: (() -> ())?) {
        RequestWithActualToken(SUCCESS: { cred in
            var parameters : Parameters = [
                    "projectId" : projectId,
                    "amount" : amount,
                    "backurl" : "https://tooba.site/home/success?shopSuccessUrl=toobaIosUrlScheme://"
            ]
            if !cardMask.isEmpty {
                parameters["cardMask"] = cardMask
            }
            let headers : HTTPHeaders = [
                    "Content-Type" : "application/x-www-form-urlencoded",
                    "Authorization" : "Bearer \(cred.access_token)",
                    "Cache-Control": "no-cache"
            ]
            let urlString = "\(Server.serverUrl)/api/pay"

            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            data.responseData(completionHandler: {
                response in
            
                switch response.result {
                case .success(let _):
                    if response.response?.statusCode == 200, let data = response.data {
                        successCompletion?(String.init(data: data, encoding: String.Encoding.utf8) ?? "")
                    } else {
                        onErrorCompletion?()
                    }
                case .failure(let value):
                    onErrorCompletion?()
                }
            })
        }, ERROR: { et, msg in
            onErrorCompletion?()
        }, UNAUTHORIZED: {
            onErrorCompletion?()
        })
    }
    
    static func SetLanguate(lang: String, SUCCESS: (() -> ())?, ERROR: (() -> ())?) {
        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = [
                "lang" : lang,
            ]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(cred.access_token)",
                "Cache-Control": "no-cache"
            ]
            let urlString = "\(Server.serverUrl)/api/account/lang"
            
            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            data.responseData(completionHandler: {
                response in
                
                switch response.result {
                case .success( _):
                    if response.response?.statusCode == 200 {
                        SUCCESS?()
                    } else {
                        ERROR?()
                    }
                case .failure(let value):
                    ERROR?()
                }
            })
        }, ERROR: { et, msg in
            ERROR?()
        }, UNAUTHORIZED: {
            ERROR?()
        })
    }

    static func YandexKassaPaymentHistory(SUCCESS: (([PaymentHistoryItem]) -> ())?, ERROR: (() -> ())?) {
        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(cred.access_token)",
                "Cache-Control": "no-cache"
            ]
            let urlString = "\(Server.serverUrl)/api/transactions"

            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            data.responseData(completionHandler: {
                response in

                switch response.result {
                case .success(let data):
                    let json = JSON.init(data: data)
                    var result: [PaymentHistoryItem] = []
                    for (_, j) in json {
                        let id = j["id"].intValue
                        let projectId = j["projectId"].intValue
                        let projectTitle = j["projectTitle"].stringValue
                        let amount = j["amount"].doubleValue
                        let currencyId = j["currencyId"].intValue
                        let paymentInfo = j["paymentInfo"].stringValue
                        let dateString = j["finishedDate"].stringValue
                        let index = dateString.index(dateString.startIndex, offsetBy: 19)
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        let date = dateFormatter.date(from: dateString.substring(to: index))
                        result.append(PaymentHistoryItem(id: id, projectId: projectId, projectTitle: projectTitle, amount: amount, currencyId: currencyId, paymentInfo: paymentInfo, date: date!))
                    }
                    SUCCESS?(result)
                case .failure(let value):
                    ERROR?()
                }
            })
        }, ERROR: { (et, msg) in
            ERROR?()
        }) {
            ERROR?()
        }
    }

    static func yandexKassaLinkedBankCards(successCompletion: (([BankCardItem]) -> ())?, onErrorCompletion: (() -> ())?) {
        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = [:]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(cred.access_token)",
                "Cache-Control": "no-cache"
            ]
            let urlString = "\(Server.serverUrl)/api/cards"

            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            data.responseData(completionHandler: {
                response in

                switch response.result {
                case .success(let data):
                    let json = JSON.init(data: data)
                    var result: [BankCardItem] = []
                    for (_,j) in json {
                        result.append(BankCardItem(cardMask: j.stringValue))
                    }
                    successCompletion?(result)
                case .failure(let value):
                    onErrorCompletion?()
                }
            })
        }, ERROR: { (et, msg) in
            onErrorCompletion?()
        }) {
            onErrorCompletion?()
        }
    }

    static func YandexKassaDeleteLinkedBankCard(cardMask: String, SUCCESS: (() -> ())?, ERROR: (() -> ())?) {
        RequestWithActualToken(SUCCESS: { cred in
            let parameters : Parameters = [
                "cardMask" : cardMask
            ]
            let headers : HTTPHeaders = [
                "Content-Type" : "application/x-www-form-urlencoded",
                "Authorization" : "Bearer \(cred.access_token)",
                "Cache-Control": "no-cache"
            ]
            let urlString = "\(Server.serverUrl)/api/cards"

            let url : URLConvertible = urlString
            let data = Alamofire.request(url, method: .delete, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            data.responseData(completionHandler: {
                response in

                switch response.result {
                case .success(let _):
                    SUCCESS?()
                case .failure(let _):
                    ERROR?()
                }
            })
        }, ERROR: { (et, msg) in
            ERROR?()
        }) {
            ERROR?()
        }
    }
}
