//
//  ProjectTableViewCell.swift
//  Tooba3
//
//  Created by Admin on 25/09/2016.
//  Copyright © 2016 greenworld. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView


class ProjectTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTag: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var txtGoal: UILabel!
    @IBOutlet weak var txtReached: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var txtPay: CurrencyLabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var lblGatheredForFinishedProject: UILabel!
    @IBOutlet weak var panelForFinished: UIStackView!
    @IBOutlet weak var panelForUnfinished: UIStackView!
    @IBOutlet weak var label_FundName: UILabel!
    @IBOutlet weak var slider: MultiImageView!
    
    //LABELS_BEGIN
    @IBOutlet weak var label_ReachedForFinished: UILabel!
    @IBOutlet weak var label_Goal: UILabel!
    @IBOutlet weak var label_ReachedForActive: UILabel!
    @IBOutlet weak var label_Help: UILabel!
    @IBOutlet weak var label_TextFinished: UILabel!
    @IBOutlet weak var button_AboutProject: UIButton!
    
    //LABELS_END
    
    public var aboutProjectPressedEvent: (() -> ())?
    public var shareButtonPressedEvent: (() -> ())?
    public var btnPayPressedEvent: ((_ amount: Double) -> ())?
    private var _project: Project? = nil
    public var startedEditing: ((_ textFieldRect: CGRect) -> ())?
    
    @IBAction func btnPayPressed(_ sender: Any) {
        txtPay.becomeFirstResponder()
    }
    @IBOutlet weak var btnPay: UIButton!
    
    @IBOutlet weak var viewWithInset: UIView!
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        shareButtonPressedEvent?()
    }
    
    @IBAction func btnAboutPressed(_ sender: Any) {
        aboutProjectPressedEvent?()
    }
    
    @IBAction func txtPayEditingBegun(_ sender: Any) {
//        let topCoordinate = txtPay.convert(txtPay.frame, to: nil).origin.y
//        print(topCoordinate)
        startedEditing?(txtPay.convert(txtPay.frame, to: nil))
    }
    
    @IBAction func pictureTouched(_ sender: Any) {
        aboutProjectPressedEvent?()
    }
    public func setProject(p: Project, row: Int = 0) {
        self.slider.setProject(p: p)
        
        self._project = p
        self.label_FundName.text = p.fundName
        self.txtPay.p = p
        self.txtPay.setValue(x: 0)
        
        self.titleView.backgroundColor = ProjectListViewController.ColorForTitleView(row: row)
        
        self.lblTag.text = "@" + p.tag.trimmingCharacters(in: ["@"]).uppercased()
        
        self.setLikeCount(likeCount: p.donatedCount)
        self.lblDescription.text = p.short_description
//        if p.photos.count > 0 {
//            self.img.kf.setImage(with: URL(string: p.photos[0].smallImageUrl))
//        }
        
        self.txtGoal.text = p.currency.toString(p.goal)
        self.txtReached.text = p.currency.toString(p.reached)
        self.lblGatheredForFinishedProject.text = p.currency.toString(p.reached)
        
        let projectFinished = p.reached >= p.goal
        panelForFinished.isHidden = !projectFinished
        panelForUnfinished.isHidden = projectFinished
        
        if p.goal <= p.reached {
            txtPay.isEnabled = false
            btnPay.isEnabled = false
            txtPay.textColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
        } else {
            txtPay.isEnabled = true
            btnPay.isEnabled = true
            txtPay.textColor = UIColor(red: 76/255.0, green: 185/255.0, blue: 241/255.0, alpha: 1.0)
        }
    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //Translation
        self.label_Goal.text = "LABEL_GOAL".localized
        self.label_Help.text = "LABEL_HELP".localized
        self.label_TextFinished.text = "TEXTFINISHED".localized
        self.label_ReachedForActive.text = "LABEL_REACHED".localized
        self.label_ReachedForFinished.text = "LABEL_REACHED".localized
        self.button_AboutProject.setTitle("BUTTON_ABOUT_PROJECT".localized, for: .normal)
        
        self.txtPay.borderStyle = .none
        self.viewWithInset.layer.cornerRadius = 8
        self.viewWithInset.layer.masksToBounds = true
        self.txtPay.payAction = { [weak self] in
            if self == nil {
                return;
            }
            self!.txtPay.resignFirstResponder()
            self!.btnPayPressedEvent?(self!.txtPay.getValue())
        }
    }
    
    @IBAction func txtPayEditing(_ sender: Any) {
        var t = 0.0
        
        if let newValue = Double(txtPay.text ?? "\(txtPay.getValue())") {
            t = newValue
        } else {
            t = txtPay.getValue()
        }
    }
    
    
    func setLikeCount(likeCount : Int) {
        lblLikeCount.text = "\(likeCount)"
//        let textWidth = lblLikeCount.intrinsicContentSize.width
//        var fr = userIcon.frame
//        fr.origin.x = lblLikeCount.frame.origin.x + lblLikeCount.frame.width - textWidth - userIcon.frame.width - 5
//        
//        userIcon.frame = fr
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
//        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
//        super.setHighlighted(highlighted, animated: animated)
    }
}
