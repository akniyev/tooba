//
//  MotivationViewController.swift
//  Tooba3
//
//  Created by Hasan Akniyev on 30/10/2017.
//  Copyright © 2017 greenworld. All rights reserved.
//

import UIKit

class MotivationViewController : UIViewController {
    weak var projectInfo: Project!
    
    static func storyboardInstance() -> MotivationViewController {
        let storyboard = UIStoryboard(name: "Payment", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MotivationViewController") as? MotivationViewController
        return vc!
    }
    
    @IBAction func closeButtonTouched(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnShareTouched(_ sender: Any) {
        // text to share
        let beginning_text = "SHARE_TEXT".localized
        let text = "\(beginning_text) @\(projectInfo.tag) https://tooba.site/m/\(projectInfo.tag)"
        
        // set up activity view controller
        let objectsToShare: [AnyObject] = [ text as AnyObject ]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        activityViewController.completionWithItemsHandler = { [unowned self] a,b,c,d in
            if b {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}
