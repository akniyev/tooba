1. You should enable Push Notifications in your app settings
2. You should go to developer.apple.com site and login with your developer account.
3. Then you should opoen your App Id and generate new developer certificate (it'll require to make certificate request from the keychain) and download it and add it to your keychain app.
4. After that you can use Pusher app (which can be installed by the command "brew cask install pusher") and select your dev certificate from keychain (which was added on the step 3) and enter your device token. 
Example payload:
{
  "aps": {
    "alert": "Breaking News!",
    "sound": "default",
    "link_url": "https://raywenderlich.com"
  }
}
